import Config from '../../../src/Config'

export const malzhausDeConfig = new Config({
  showtimes: {
    url: 'https://www.malzhaus.de/programm/kino',
    showtimes: {
      box: 'table.eventlist > tbody > tr',
      movieTitle: {
        selector: '.col-event a',
        mapper: (value: string) => value.split('Regie')[0].trim()
      },
      directors: (box: cheerio.Cheerio) => {
        const dirBox = box.find('.col-event a').html()
        const directors = dirBox
          ?.split('<br>')
          .filter(x => /regie/gi.test(x))
          .map(x => x.replace(/regie:|\(.*\)/gi, '').trim().split(','))
          .flat()
        return directors
      },
      releaseDate: (box: cheerio.Cheerio) => {
        const releaseBox = box.find('.col-event a').html()
        const releaseData = releaseBox?.split('<br>').filter(x => /\d{4}/gi.test(x))[0]
        const releaseDateMatch = releaseData?.match(/\d{4}/gi)?.[0]
        return releaseDateMatch
      },
      releaseDateFormat: 'YYYY',
      date: {
        selector: '.col-date ',
        mapper: (text: string) => text.match(/\d{2}.\d{2}./)?.[0].trim()
      },
      time: {
        selector: '.col-date ',
        mapper: (text: string) => text.match(/\d{2}.\d{2}\sUhr/)?.[0].trim()
      },
      timeFormat: 'HH.mm U\\hr',
      dateFormat: 'DD.MM.',
      dateLocale: 'de'
    }
  }
})