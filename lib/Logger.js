"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerProxy = exports.SilentLogger = exports.DefaultLogger = void 0;
var debug = require("debug");
var util = require("util");
var colors = require("colors");
var _ = require("underscore");
var Constants_1 = require("./Constants");
/** @private */
var LOG_STYLES = {
    emoji: {
        warn: '⚠️ ',
        error: '❗️ '
    },
    short: {
        warn: 'W:',
        error: 'E:'
    },
    long: {
        warn: 'WARNING:',
        error: 'ERROR:'
    }
};
/**
 * Default Logger class. Logs as following:
 * - debug →   the [debug](https://github.com/visionmedia/debug) package. See [Debug Logs](https://crawl-e.internationalshowtimes.com/#/basics/debug-logs) in the framework documentation.
 * - info →    `console.log()` in default color
 * - warning → `console.log()` in yellow color
 * - error →   `console.log()` in red color
 */
var DefaultLogger = /** @class */ (function () {
    function DefaultLogger() {
        this.logStyle = LOG_STYLES.emoji;
    }
    /**
     * Logs a message via the debug module
     * @param debugPrefix prefix for the debug module
     * @param msg
     */
    DefaultLogger.prototype.debug = function (debugPrefix) {
        var msg = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            msg[_i - 1] = arguments[_i];
        }
        debugPrefix = _.compact([Constants_1.default.MAIN_DEBUG_PREFIX, debugPrefix]).join(':');
        debug(debugPrefix).apply(void 0, msg);
    };
    DefaultLogger.prototype.info = function (message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        console.info(util.format.apply(util, __spreadArray([message], optionalParams, false)));
    };
    DefaultLogger.prototype.warn = function (message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        console.warn(colors.yellow(this.logStyle.warn), colors.yellow(util.format.apply(util, __spreadArray([message], optionalParams, false))));
    };
    DefaultLogger.prototype.error = function (message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        console.error(colors.red(this.logStyle.error), colors.red(util.format.apply(util, __spreadArray([message], optionalParams, false))));
    };
    return DefaultLogger;
}());
exports.DefaultLogger = DefaultLogger;
/**
 * Placeholder Logger implemenation that actually logs nothing.
 */
var SilentLogger = /** @class */ (function () {
    function SilentLogger() {
    }
    SilentLogger.prototype.debug = function (debugPrefix, msg) { };
    SilentLogger.prototype.info = function (message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
    };
    SilentLogger.prototype.warn = function (message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
    };
    SilentLogger.prototype.error = function (message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
    };
    return SilentLogger;
}());
exports.SilentLogger = SilentLogger;
/**
 * Object to bridges a reference to a logger, so that the underlying logger can be changed anywhere for all usages.
 */
var LoggerProxy = /** @class */ (function () {
    function LoggerProxy(logger) {
        this.logger = logger || new SilentLogger();
    }
    LoggerProxy.prototype.debug = function (name, message) {
        var _a;
        var optionalParams = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            optionalParams[_i - 2] = arguments[_i];
        }
        (_a = this.logger).debug.apply(_a, __spreadArray([name, message], optionalParams, false));
    };
    LoggerProxy.prototype.info = function (message) {
        var _a;
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        (_a = this.logger).info.apply(_a, __spreadArray([message], optionalParams, false));
    };
    LoggerProxy.prototype.warn = function (message) {
        var _a;
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        (_a = this.logger).warn.apply(_a, __spreadArray([message], optionalParams, false));
    };
    LoggerProxy.prototype.error = function (message) {
        var _a;
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        (_a = this.logger).error.apply(_a, __spreadArray([message], optionalParams, false));
    };
    return LoggerProxy;
}());
exports.LoggerProxy = LoggerProxy;
