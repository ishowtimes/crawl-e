"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("underscore");
var ObjectPath = require("object-path");
var config_schema_1 = require("./config-schema");
var Utils_1 = require("./Utils");
var Logger_1 = require("./Logger");
/** @private */
function parseListConfig(listConfig) {
    var result = _.clone(listConfig);
    result.urls = (listConfig === null || listConfig === void 0 ? void 0 : listConfig.urls) || [listConfig === null || listConfig === void 0 ? void 0 : listConfig.url];
    result.urlDateCount = listConfig.urlDateCount || 14;
    return result;
}
// https://stackoverflow.com/a/21557600/1879171
/** @private */
function permutate(src, minLen, maxLen) {
    minLen = minLen - 1 || 0;
    maxLen = maxLen || src.length + 1;
    var Asource = src.slice(); // copy the original so we don't apply results to the original.
    var Aout = [];
    var minMax = function (arr) {
        var len = arr.length;
        if (len > minLen && len <= maxLen) {
            Aout.push(arr);
        }
    };
    var picker = function (arr, holder, collect) {
        if (holder.length) {
            collect.push(holder);
        }
        var len = arr.length;
        for (var i = 0; i < len; i++) {
            var arrcopy = arr.slice();
            var elem = arrcopy.splice(i, 1);
            var result = holder.concat(elem);
            minMax(result);
            if (len) {
                picker(arrcopy, result, collect);
            }
            else {
                collect.push(result);
            }
        }
    };
    picker(Asource, [], []);
    return Aout;
}
/**
 * Default Config implementation.
 * @category Main
 */
var Config = /** @class */ (function () {
    function Config(config, logger) {
        if (logger === void 0) { logger = new Logger_1.SilentLogger(); }
        var _this = this;
        this.crawler = {};
        var pickGeneralValue = function (key) {
            _this[key] = _.has(config, key)
                ? config[key]
                : config_schema_1.default.properties[key].default;
        };
        pickGeneralValue('proxyUri');
        pickGeneralValue('concurrency');
        pickGeneralValue('useRandomUserAgent');
        pickGeneralValue('timezone');
        this.acceptedWarnings = (config === null || config === void 0 ? void 0 : config.acceptedWarnings) || {};
        this.isTemporarilyClosed = config === null || config === void 0 ? void 0 : config.isTemporarilyClosed;
        if ((config === null || config === void 0 ? void 0 : config.cinemas) && (config === null || config === void 0 ? void 0 : config.cinemas.constructor) === Array) {
            this.cinemas = config === null || config === void 0 ? void 0 : config.cinemas;
        }
        else if (config === null || config === void 0 ? void 0 : config.cinemas) {
            this.cinemasConfig = {
                list: (config === null || config === void 0 ? void 0 : config.cinemas.list)
                    ? parseListConfig(config === null || config === void 0 ? void 0 : config.cinemas.list)
                    : undefined,
                details: _.clone(config === null || config === void 0 ? void 0 : config.cinemas.details)
            };
        }
        if (config === null || config === void 0 ? void 0 : config.showtimes) {
            config.showtimes = (config === null || config === void 0 ? void 0 : config.showtimes.constructor) === Array
                ? config === null || config === void 0 ? void 0 : config.showtimes
                : [config === null || config === void 0 ? void 0 : config.showtimes];
            this.showtimes = config === null || config === void 0 ? void 0 : config.showtimes.map(function (showtimesConfig) { return parseListConfig(showtimesConfig); });
            this.showtimes.forEach(function (showtimesConfig, index) {
                if (showtimesConfig === null || showtimesConfig === void 0 ? void 0 : showtimesConfig.showtimes) {
                    showtimesConfig.showtimes = _this.parseShowtimesParsingConfig(showtimesConfig === null || showtimesConfig === void 0 ? void 0 : showtimesConfig.showtimes);
                }
                var parsingConfigKeyPaths = _this.generateParsingConfigKeyPaths("showtimes.".concat(index));
                // resolve showtimes parsing configs
                parsingConfigKeyPaths
                    .map(function (keypath) { return [keypath, 'showtimes'].join('.'); })
                    .forEach(function (keypath) {
                    if (ObjectPath.get(config, keypath)) {
                        ObjectPath.set(_this, keypath, _this.parseShowtimesParsingConfig(ObjectPath.get(_this, keypath)));
                    }
                });
                // resolve showtimes table configs
                _this.resolveTableConfig(config, parsingConfigKeyPaths.concat("showtimes.".concat(index)), logger);
                // resolve default box selectors
                parsingConfigKeyPaths
                    .filter(function (keypath) { return keypath.match(/periods$/i); })
                    .forEach(function (keypath) {
                    if (ObjectPath.get(config, keypath)) {
                        var boxKeyPath = [keypath, 'box'].join('.');
                        ObjectPath.set(_this, boxKeyPath, ObjectPath.get(_this, boxKeyPath) || 'body');
                    }
                });
            });
        }
        var crawlingConfigKeys = ['movies', 'dates'];
        crawlingConfigKeys.forEach(function (crawlingConfigKey) {
            if (_.has(config, crawlingConfigKey)) {
                _this[crawlingConfigKey] = {
                    list: parseListConfig(config[crawlingConfigKey].list),
                    showtimes: undefined
                };
                if (config[crawlingConfigKey].showtimes) {
                    _this[crawlingConfigKey].showtimes = parseListConfig(config[crawlingConfigKey].showtimes);
                    // resolve showtimes table configs
                    var parsingConfigKeyPaths_1 = _this.generateParsingConfigKeyPaths("".concat(crawlingConfigKey, ".showtimes"));
                    _this.resolveTableConfig(config, parsingConfigKeyPaths_1.concat("".concat(crawlingConfigKey, ".showtimes")), logger);
                }
                var parsingConfigKeyPaths = _this.generateParsingConfigKeyPaths("".concat(crawlingConfigKey, ".showtimes"));
                // resolve showtimes parsing configs
                parsingConfigKeyPaths
                    .map(function (keypath) { return [keypath, 'showtimes'].join('.'); })
                    .forEach(function (keypath) {
                    if (ObjectPath.get(config, keypath)) {
                        ObjectPath.set(_this, keypath, _this.parseShowtimesParsingConfig(ObjectPath.get(_this, keypath)));
                    }
                });
            }
        });
        this.hooks = (config === null || config === void 0 ? void 0 : config.hooks) || {};
        this.setCrawlerConfig(config);
    }
    /**
     * Generates a list of all possible keypath combiniations
     * that could be configured for box & showtimes parsing
     * relative to the given parentKeyPath.
     *
     * see corresponding mocha test checking all sorts of combinations
     */
    Config.prototype.generateParsingConfigKeyPaths = function (parentKeyPath) {
        var parsingConfigs = ['movies', 'dates', 'periods', 'auditoria', 'versions', 'forEach'];
        return permutate(parsingConfigs, 0, 0).map(function (combi) { return [parentKeyPath].concat(combi).join('.'); });
    };
    Config.prototype.resolveTableConfig = function (config, parsingConfigKeyPaths, logger) {
        var _this = this;
        parsingConfigKeyPaths
            .forEach(function (keypath) {
            if (ObjectPath.get(config, keypath + '.table.cells.showtimes')) {
                ObjectPath.set(_this, keypath + '.table.cells.showtimes', _this.parseShowtimesParsingConfig(ObjectPath.get(_this, keypath + '.table.cells.showtimes')));
            }
        });
    };
    Config.prototype.setCrawlerConfig = function (config) {
        var _a, _b;
        this.crawler.id = ((config === null || config === void 0 ? void 0 : config.crawler) && ((_a = config === null || config === void 0 ? void 0 : config.crawler) === null || _a === void 0 ? void 0 : _a.id)) || Utils_1.default.getMainFilenameBase();
        if ((config === null || config === void 0 ? void 0 : config.crawler) && typeof (config === null || config === void 0 ? void 0 : config.crawler.is_booking_link_capable) === 'boolean') {
            this.crawler.is_booking_link_capable = config === null || config === void 0 ? void 0 : config.crawler.is_booking_link_capable;
        }
        this.crawler.is_booking_link_capable = this.crawler.is_booking_link_capable || false;
        if ((config === null || config === void 0 ? void 0 : config.crawler) && typeof ((_b = config === null || config === void 0 ? void 0 : config.crawler) === null || _b === void 0 ? void 0 : _b.jira_issues)) {
            this.crawler.jira_issues = config === null || config === void 0 ? void 0 : config.crawler.jira_issues;
        }
    };
    Config.prototype.parseShowtimesParsingConfig = function (showtimesConfig) {
        if (!showtimesConfig) {
            return showtimesConfig;
        }
        var result = _.clone(showtimesConfig);
        if (Utils_1.default.isLinkTagSelector(showtimesConfig === null || showtimesConfig === void 0 ? void 0 : showtimesConfig.box)) {
            this.crawler.is_booking_link_capable = true;
        }
        if (showtimesConfig === null || showtimesConfig === void 0 ? void 0 : showtimesConfig.bookingLink) {
            this.crawler.is_booking_link_capable = true;
        }
        return result;
    };
    return Config;
}());
exports.default = Config;
