"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
var Constants_1 = require("./Constants");
var debug = require("debug");
var util = require("util");
var _debbuPrefix = "".concat(Constants_1.default.MAIN_DEBUG_PREFIX, ":callstack");
var _logMethodCallDebug = debug(_debbuPrefix);
var MethodCallLogger;
(function (MethodCallLogger) {
    function currentMethodName(offset) {
        if (offset === void 0) { offset = 0; }
        var err = new Error();
        var methodName = err.stack.split('\n')[3 + offset].match(/(at )([\w|.]+)/)[2];
        return methodName;
    }
    MethodCallLogger.currentMethodName = currentMethodName;
    function logMethodCall() {
        var optionalParams = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            optionalParams[_i] = arguments[_i];
        }
        _logMethodCallDebug(util.format.apply(util, __spreadArray([currentMethodName()], optionalParams, false)));
    }
    MethodCallLogger.logMethodCall = logMethodCall;
})(MethodCallLogger || (MethodCallLogger = {}));
exports.default = MethodCallLogger;
