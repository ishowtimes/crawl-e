"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ImdbIdParser = void 0;
var MethodCallLogger_1 = require("../MethodCallLogger");
var BaseParser_1 = require("./BaseParser");
/**
 * ImdbIdParser
 * @category HTML Parsing
 */
var ImdbIdParser = /** @class */ (function (_super) {
    __extends(ImdbIdParser, _super);
    function ImdbIdParser() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.contextKey = 'imdbId';
        return _this;
    }
    /**
     * Parses a single imdb id from a HTML box.
     * @param imdbIdBox the HTML box to grab the imdb id string from
     * @param imdbIdParsingConfig config for grabbing the imdb id string and parsing it
     * @param context the context which to set the `imdb id` property on
     * @returns the parsed imdb id as string
     */
    ImdbIdParser.prototype.parse = function (imdbIdBox, imdbIdParsingConfig, context) {
        MethodCallLogger_1.default.logMethodCall();
        context.pushCallstack();
        context.movie = context.movie || {};
        context.movie.imdbId = this.resovleValueGrabber('imdbId', imdbIdParsingConfig).grabFirst(imdbIdBox, context);
        context.popCallstack();
        return context.movie.imdbId;
    };
    return ImdbIdParser;
}(BaseParser_1.BaseParser));
exports.ImdbIdParser = ImdbIdParser;
