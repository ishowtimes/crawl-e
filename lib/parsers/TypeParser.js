"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypeParser = void 0;
var ValueGrabber_1 = require("../ValueGrabber");
var BaseParser_1 = require("./BaseParser");
var MethodCallLogger_1 = require("../MethodCallLogger");
/**
 * Parser for `type`
 * @category HTML Parsing
 */
var TypeParser = /** @class */ (function (_super) {
    __extends(TypeParser, _super);
    function TypeParser() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.contextKey = 'type';
        return _this;
    }
    /**
     * Parses a HTML box and type into the given context's property.
     * @param box
     * @param config
     * @param context the context which to set the type details on
     * @returns the parsed type details
     */
    TypeParser.prototype.parse = function (box, parsingConfig, context) {
        MethodCallLogger_1.default.logMethodCall();
        context.pushCallstack();
        context.movie = context.movie || {};
        context.movie.type = this.parseType(box, parsingConfig, context);
        context.popCallstack();
        return context.movie.type;
    };
    TypeParser.prototype.parseType = function (box, parsingConfig, context) {
        var _a;
        MethodCallLogger_1.default.logMethodCall();
        context.pushCallstack();
        var type = (_a = context.movie) === null || _a === void 0 ? void 0 : _a.type;
        if (parsingConfig.type) {
            var valueGrabber = new ValueGrabber_1.default(parsingConfig.type, this.logger, this.valueGrabberLogPrefix('type'));
            type = valueGrabber.grabFirst(box, context);
        }
        context.popCallstack();
        return type;
    };
    return TypeParser;
}(BaseParser_1.BaseParser));
exports.TypeParser = TypeParser;
