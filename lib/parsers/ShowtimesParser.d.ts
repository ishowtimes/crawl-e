/// <reference types="cheerio" />
import * as moment from 'moment';
import Logger from '../Logger';
import { CallbackFunction } from '../ResponseParsers';
import { SubConfigs } from '../Config';
import { ParsingContext } from './ParsingContext';
import { LanguageParsingConfig } from './LanguageParser';
import { DateParser, DatesIValueGrabver } from './DateParser';
import { TimeParsingConfig, TimeParser } from './TimeParser';
import { VersionParsingConfig, VersionParser } from './VersionParser';
import { AuditoriumParsingConfig } from './AuditoriumParser';
import { IndirectMovieParsingConfig } from './MovieParser';
import { ValueGrabbing } from '../ValueGrabber';
import { BaseParser } from './BaseParser';
import { DirectorsParser } from './DirectorsParser';
import { ReleaseDateParser } from './ReleaseDateParser';
/**
 * ShowtimeItemParsingConfig
 * @category HTML Parsing
 */
export interface ShowtimeItemParsingConfig extends LanguageParsingConfig, TimeParsingConfig, VersionParsingConfig, AuditoriumParsingConfig, IndirectMovieParsingConfig {
    /** Value Grabber for the date string */
    date?: ValueGrabbing;
    dates?: DatesIValueGrabver;
    dateFormat?: string;
    dateLocale?: string;
    datetimeParsing?: boolean;
    datetimeFormat?: string;
    datetimeLocale?: string;
    preserveYear?: boolean;
    bookingLink?: ValueGrabbing;
    releaseDate?: ValueGrabbing;
    releaseDateFormat?: moment.MomentFormatSpecification;
    releaseDateLocale?: string;
    directors?: ValueGrabbing;
}
/**
 * ShowtimesParser
 * @category HTML Parsing
 */
export declare class ShowtimesParser extends BaseParser {
    private dateParser;
    private timeParser;
    private versionParser;
    private directorsParser;
    private releaseDateParser;
    constructor(logger: Logger, dateParser: DateParser, timeParser: TimeParser, versionParser: VersionParser, directorsParser: DirectorsParser, releaseDateParser: ReleaseDateParser);
    parseShowtimes(showtimesContainer: cheerio.Cheerio, showtimesConfig: SubConfigs.Showtimes.ListParsing, context: ParsingContext, callback: CallbackFunction): void;
    private parseItems;
    private parseShowtime;
    private dateParsingConfig;
    private resolveFallbacksAndDefaults;
    private releaseDateParsingConfig;
}
