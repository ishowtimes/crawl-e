/// <reference types="cheerio" />
import Logger from '../Logger';
import { ItemParser } from './ItemParser';
import { ParsingContext } from './ParsingContext';
import { ValueGrabbing } from '../ValueGrabber';
import { BaseParser } from './BaseParser';
/**
 * DirectorsParsingConfig
 * @category HTML Parsing
 */
export interface DirectorsParsingConfig {
    directors?: ValueGrabbing;
}
/**
 * Parses directors`.
 * @category HTML Parsing
 */
export declare class DirectorsParser extends BaseParser implements ItemParser<DirectorsParsingConfig> {
    constructor(logger: Logger);
    contextKey: string;
    /**
     * Parses directors details from a HTML box and merges them into the given context's `movie` property.
     * @param box
     * @param parsingConfig
     * @param context
     * @returns the merged movie object as in the context after the parsing
     */
    parse(box: cheerio.Cheerio, parsingConfig: DirectorsParsingConfig, context: ParsingContext): string[];
    parseDirectors(box: cheerio.Cheerio, parsingConfig: DirectorsParsingConfig, context: ParsingContext): string[];
}
