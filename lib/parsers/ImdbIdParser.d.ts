/// <reference types="cheerio" />
import { ParsingContext } from './ParsingContext';
import { ItemParser } from './ItemParser';
import { ValueGrabbing } from '../ValueGrabber';
import { BaseParser } from './BaseParser';
export interface ImdbIdParsingConfig {
    /**
     * Value Grabber for the imdb id.
     */
    imdbId?: ValueGrabbing;
}
/**
 * ImdbIdParser
 * @category HTML Parsing
 */
export declare class ImdbIdParser extends BaseParser implements ItemParser<ImdbIdParsingConfig> {
    contextKey: string;
    /**
     * Parses a single imdb id from a HTML box.
     * @param imdbIdBox the HTML box to grab the imdb id string from
     * @param imdbIdParsingConfig config for grabbing the imdb id string and parsing it
     * @param context the context which to set the `imdb id` property on
     * @returns the parsed imdb id as string
     */
    parse(imdbIdBox: cheerio.Cheerio, imdbIdParsingConfig: ImdbIdParsingConfig, context: ParsingContext): string;
}
