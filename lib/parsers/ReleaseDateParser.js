"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReleaseDateParser = exports.styleMom = exports.styleStr = void 0;
var moment = require("moment");
var colors = require("colors");
var Constants_1 = require("../Constants");
var MethodCallLogger_1 = require("../MethodCallLogger");
var BaseParser_1 = require("./BaseParser");
/** @private */
var styleStr = function (str) { return str
    ? colors.underline.green(str)
    : colors.inverse.red('null'); };
exports.styleStr = styleStr;
/** @private */
var styleMom = function (m, format) {
    var color = m.isValid() ? colors.green : colors.red;
    return colors.bold(color(m.format(format)));
};
exports.styleMom = styleMom;
/**
 * ReleaseDateParser
 * @category HTML Parsing
 */
var ReleaseDateParser = /** @class */ (function (_super) {
    __extends(ReleaseDateParser, _super);
    function ReleaseDateParser() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.debugKey = 'selection:releaseDate:parsing';
        _this.contextKey = 'releaseDate';
        _this.logPrefix = 'releaseDate';
        return _this;
    }
    /**
     * Parses a single release date from a HTML box.
     * @param releaseDateBox the HTML box to grab the release date string from
     * @param releaseDateParsingConfig config for grabbing the release date string and parsing it
     * @param context the context which to set the `release date` property on
     * @returns the parsed release date as moment object
     */
    ReleaseDateParser.prototype.parse = function (releaseDateBox, releaseDateParsingConfig, context) {
        MethodCallLogger_1.default.logMethodCall();
        context.pushCallstack();
        var releaseDateStr = this.resovleValueGrabber('releaseDate', releaseDateParsingConfig).grabFirst(releaseDateBox, context);
        var releaseDate = this.parseReleaseDateStr(releaseDateStr, releaseDateParsingConfig, context);
        context.popCallstack();
        return releaseDate;
    };
    /**
     * Parses a release date string.
     * @param releaseDateStr the formatted release date string to parse
     * @param releaseDateParsingConfig config for parsing the release date string
     * @param context the context which to set the `release date` property on
     * @returns the parsed release date as moment object
     */
    ReleaseDateParser.prototype.parseReleaseDateStr = function (releaseDateStr, releaseDateParsingConfig, context) {
        MethodCallLogger_1.default.logMethodCall();
        context.pushCallstack();
        var releaseDate;
        function testMatchAnyWords(releaseDateStr, words) {
            if (typeof releaseDateStr !== 'string') {
                return false;
            }
            if (words.indexOf(releaseDateStr.trim()) !== -1) {
                return true;
            }
            var regex = new RegExp('\\b(' + words
                .map(function (w) { return "(".concat(w, ")"); })
                .join('|') + ')\\b', 'gi');
            return regex.test(releaseDateStr.trim());
        }
        releaseDate = moment(releaseDateStr, releaseDateParsingConfig.releaseDateFormat, releaseDateParsingConfig.releaseDateLocale, true);
        if (!releaseDate.isValid() && testMatchAnyWords(releaseDateStr, Constants_1.default.DAY_AFTER_TOMORROW_WORS)) {
            releaseDate = moment().add(2, 'days').set('hour', 0);
            this.logger.debug(this.debugKey, 'releaseDateStr:', (0, exports.styleStr)(releaseDateStr), '-> matched regex for day after tomorrow', colors.bold(' => '), (0, exports.styleMom)(releaseDate, 'YYYY-MM-DD'));
        }
        else if (!releaseDate.isValid() && testMatchAnyWords(releaseDateStr, Constants_1.default.TOMORROW_WORS)) {
            releaseDate = moment().add(1, 'day').set('hour', 0);
            this.logger.debug(this.debugKey, 'releaseDateStr:', (0, exports.styleStr)(releaseDateStr), '-> matched tomorrow regex', colors.bold(' => '), (0, exports.styleMom)(releaseDate, 'YYYY-MM-DD'));
        }
        else if (!releaseDate.isValid() && testMatchAnyWords(releaseDateStr, Constants_1.default.TODAY_WORDS)) {
            releaseDate = moment().set('hour', 0);
            this.logger.debug(this.debugKey, 'releaseDateStr:', (0, exports.styleStr)(releaseDateStr), '-> matched today regex', colors.bold(' => '), (0, exports.styleMom)(releaseDate, 'YYYY-MM-DD'));
        }
        else {
            this.logger.debug(this.debugKey, 'releaseDateStr:', (0, exports.styleStr)(releaseDateStr), 'format:', (0, exports.styleStr)(releaseDateParsingConfig.releaseDateFormat), 'locale:', (0, exports.styleStr)(releaseDateParsingConfig.releaseDateLocale), colors.bold(' => '), (0, exports.styleMom)(releaseDate, 'YYYY-MM-DD'));
        }
        context.popCallstack();
        return releaseDate;
    };
    return ReleaseDateParser;
}(BaseParser_1.BaseParser));
exports.ReleaseDateParser = ReleaseDateParser;
