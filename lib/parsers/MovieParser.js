"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.MovieParser = void 0;
var MethodCallLogger_1 = require("../MethodCallLogger");
var Utils_1 = require("../Utils");
var BaseParser_1 = require("./BaseParser");
var ReleaseDateParser_1 = require("./ReleaseDateParser");
var moment = require("moment");
var TypeParser_1 = require("./TypeParser");
var ImdbIdParser_1 = require("./ImdbIdParser");
var MoviePageLinkParser_1 = require("./MoviePageLinkParser");
/**
 * @category HTML Parsing
 */
var MovieParser = /** @class */ (function (_super) {
    __extends(MovieParser, _super);
    function MovieParser(logger, versionParser, directorsParser, releaseDateParser) {
        var _this = _super.call(this, logger) || this;
        _this.versionParser = versionParser;
        _this.directorsParser = directorsParser;
        _this.releaseDateParser = releaseDateParser;
        _this.contextKey = 'movie';
        return _this;
    }
    MovieParser.prototype.parse = function (movieBox, parsingConfig, context) {
        MethodCallLogger_1.default.logMethodCall();
        context.pushCallstack();
        parsingConfig.title = parsingConfig.movieTitle || parsingConfig.title;
        parsingConfig.titleOriginal = parsingConfig.movieTitleOriginal || parsingConfig.titleOriginal;
        this.parsingConfig = parsingConfig;
        context.movie = {};
        context.movie.id = this.grabProperty('id', movieBox, context);
        context.movie.title = this.grabProperty('title', movieBox, context);
        context.movie.titleOriginal = this.grabProperty('titleOriginal', movieBox, context);
        context.movie.href = this.grabProperty('href', movieBox, context);
        context.movie = Utils_1.default.compactObj(context.movie);
        var directorsParsingConfig = { directors: parsingConfig.directors };
        context.movie.directors = this.directorsParser.parseDirectors(movieBox, directorsParsingConfig, context);
        var typeParsingConfig = { type: parsingConfig.type };
        var typeParser = new TypeParser_1.TypeParser(this.logger);
        context.movie.type = typeParser.parseType(movieBox, typeParsingConfig, context) || this.grabProperty('type', movieBox, context);
        this.releaseDateParser = new ReleaseDateParser_1.ReleaseDateParser(this.logger);
        var releaseDateParsingConfig = this.releaseDateParsingConfig(parsingConfig);
        var releaseDate = this.releaseDateParser.parse(movieBox, releaseDateParsingConfig, context);
        var format = releaseDateParsingConfig === null || releaseDateParsingConfig === void 0 ? void 0 : releaseDateParsingConfig.releaseDateFormat;
        var strFormat = format === null || format === void 0 ? void 0 : format.toString();
        var isValidMoment = moment.isMoment(releaseDate);
        var isValidReleaseDate = releaseDate.isValid();
        if (!isValidMoment) {
            this.logger.debug("movies:selection", "release date is not a valid moment value");
        }
        else if (!isValidReleaseDate) {
            this.logger.debug("movies:selection", "skipping movies due to invalid release date");
        }
        else if (strFormat) {
            switch (true) {
                case !!strFormat.match(/D+/):
                    context.movie.releaseDate = releaseDate.format('YYYY-MM-DD');
                    break;
                case !!strFormat.match(/M+/):
                    context.movie.releaseDate = releaseDate.format('YYYY-MM');
                    break;
                default:
                    context.movie.releaseDate = releaseDate.format('YYYY');
                    break;
            }
        }
        else {
            this.logger.debug("movies:selection", "Unknown case for format of release date");
        }
        if (parsingConfig.imdbId) {
            var imdbIdParsingConfig = { imdbId: parsingConfig.imdbId };
            var imdbIdParser = new ImdbIdParser_1.ImdbIdParser(this.logger);
            context.movie.imdbId = imdbIdParser.parse(movieBox, imdbIdParsingConfig, context) || this.grabProperty('imdbId', movieBox, context);
        }
        if (parsingConfig.moviePageLink) {
            var moviePageLinkParsingConfig = { moviePageLink: parsingConfig.moviePageLink };
            var moviePageLinkParser = new MoviePageLinkParser_1.MoviePageLinkParser(this.logger);
            context.movie.moviePageLink = moviePageLinkParser.parse(movieBox, moviePageLinkParsingConfig, context) || this.grabProperty('moviePageLink', movieBox, context);
        }
        context.movie.version = this.versionParser.parse(movieBox, parsingConfig, context);
        this.logger.debug("movies:result", context.movie);
        context.popCallstack();
        return context.movie;
    };
    MovieParser.prototype.releaseDateParsingConfig = function (moviesConfig) {
        return {
            releaseDate: moviesConfig.releaseDate,
            releaseDateFormat: moviesConfig.releaseDateFormat || 'YYYY',
            releaseDateLocale: moviesConfig.releaseDateLocale || 'en'
        };
    };
    return MovieParser;
}(BaseParser_1.BaseParser));
exports.MovieParser = MovieParser;
