"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.MoviePageLinkParser = void 0;
var MethodCallLogger_1 = require("../MethodCallLogger");
var BaseParser_1 = require("./BaseParser");
/**
 * moviePageLinkParser
 * @category HTML Parsing
 */
var MoviePageLinkParser = /** @class */ (function (_super) {
    __extends(MoviePageLinkParser, _super);
    function MoviePageLinkParser() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.contextKey = 'moviePageLink';
        return _this;
    }
    /**
     * Parses a single movie page link from a HTML box.
     * @param moviePageLinkBox the HTML box to grab the movie page link string from
     * @param moviePageLinkParsingConfig config for grabbing the movie page link string and parsing it
     * @param context the context which to set the `movie page link` property on
     * @returns the parsed movie page link as string
     */
    MoviePageLinkParser.prototype.parse = function (moviePageLinkBox, moviePageLinkParsingConfig, context) {
        MethodCallLogger_1.default.logMethodCall();
        context.pushCallstack();
        context.movie = context.movie || {};
        context.movie.moviePageLink = this.resovleValueGrabber('moviePageLink', moviePageLinkParsingConfig).grabFirst(moviePageLinkBox, context);
        context.popCallstack();
        return context.movie.moviePageLink;
    };
    return MoviePageLinkParser;
}(BaseParser_1.BaseParser));
exports.MoviePageLinkParser = MoviePageLinkParser;
