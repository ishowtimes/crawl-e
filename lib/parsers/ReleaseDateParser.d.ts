/// <reference types="cheerio" />
import * as moment from 'moment';
import { ParsingContext } from './ParsingContext';
import { ItemParser } from './ItemParser';
import { ValueGrabbing } from '../ValueGrabber';
import { BaseParser } from './BaseParser';
/** @private */
export declare const styleStr: (str: any) => string;
/** @private */
export declare const styleMom: (m: moment.Moment, format: string) => string;
export type ReleaseDate = string;
/**
 * @category HTML Parsing
 */
export interface ReleaseDateStringParsingConfig {
    /**
     * Format(s) for parsing a release date string selected by the `release date` ValueGrabber
    */
    releaseDateFormat?: moment.MomentFormatSpecification;
    /**
     * Locale for parsing a release date string. (Specified as [ISO 639 code](http://www.localeplanet.com/icu/iso639.html))
    */
    releaseDateLocale?: string;
    /**
     * Disables year correction of release date without explicit year information."
     * @default false
    */
    preserveYear?: boolean;
}
/**
 * @category HTML Parsing
 */
export interface ReleaseDateParsingConfig extends ReleaseDateStringParsingConfig {
    /**
     * Value Grabber for the release date string.
     */
    releaseDate?: ValueGrabbing;
}
/**
 * ReleaseDateParser
 * @category HTML Parsing
 */
export declare class ReleaseDateParser extends BaseParser implements ItemParser<ReleaseDateParsingConfig> {
    debugKey: string;
    contextKey: string;
    logPrefix: string;
    /**
     * Parses a single release date from a HTML box.
     * @param releaseDateBox the HTML box to grab the release date string from
     * @param releaseDateParsingConfig config for grabbing the release date string and parsing it
     * @param context the context which to set the `release date` property on
     * @returns the parsed release date as moment object
     */
    parse(releaseDateBox: cheerio.Cheerio, releaseDateParsingConfig: ReleaseDateParsingConfig, context: ParsingContext): moment.Moment;
    /**
     * Parses a release date string.
     * @param releaseDateStr the formatted release date string to parse
     * @param releaseDateParsingConfig config for parsing the release date string
     * @param context the context which to set the `release date` property on
     * @returns the parsed release date as moment object
     */
    parseReleaseDateStr(releaseDateStr: string, releaseDateParsingConfig: ReleaseDateStringParsingConfig, context: ParsingContext): moment.Moment;
}
