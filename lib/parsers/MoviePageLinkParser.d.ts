/// <reference types="cheerio" />
import { ParsingContext } from './ParsingContext';
import { ItemParser } from './ItemParser';
import { ValueGrabbing } from '../ValueGrabber';
import { BaseParser } from './BaseParser';
export interface MoviePageLinkParsingConfig {
    /**
     * Value Grabber for the movie page link.
     */
    moviePageLink?: ValueGrabbing;
}
/**
 * moviePageLinkParser
 * @category HTML Parsing
 */
export declare class MoviePageLinkParser extends BaseParser implements ItemParser<MoviePageLinkParsingConfig> {
    contextKey: string;
    /**
     * Parses a single movie page link from a HTML box.
     * @param moviePageLinkBox the HTML box to grab the movie page link string from
     * @param moviePageLinkParsingConfig config for grabbing the movie page link string and parsing it
     * @param context the context which to set the `movie page link` property on
     * @returns the parsed movie page link as string
     */
    parse(moviePageLinkBox: cheerio.Cheerio, moviePageLinkParsingConfig: MoviePageLinkParsingConfig, context: ParsingContext): string;
}
