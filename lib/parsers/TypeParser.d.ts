/// <reference types="cheerio" />
import { ParsingContext } from './ParsingContext';
import { ItemParser } from './ItemParser';
import { ValueGrabbing } from '../ValueGrabber';
import { BaseParser } from './BaseParser';
/**
 * @category HTML Parsing
 */
export interface TypeParsingConfig {
    type?: ValueGrabbing;
}
/**
 * Parser for `type`
 * @category HTML Parsing
 */
export declare class TypeParser extends BaseParser implements ItemParser<TypeParsingConfig> {
    contextKey: string;
    /**
     * Parses a HTML box and type into the given context's property.
     * @param box
     * @param config
     * @param context the context which to set the type details on
     * @returns the parsed type details
     */
    parse(box: cheerio.Cheerio, parsingConfig: TypeParsingConfig, context: ParsingContext): string;
    parseType(box: cheerio.Cheerio, parsingConfig: TypeParsingConfig, context: ParsingContext): string;
}
