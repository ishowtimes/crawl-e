"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DirectorsParser = void 0;
var _ = require("underscore");
var MethodCallLogger_1 = require("../MethodCallLogger");
var ValueGrabber_1 = require("../ValueGrabber");
var BaseParser_1 = require("./BaseParser");
/**
 * Parses directors`.
 * @category HTML Parsing
 */
var DirectorsParser = /** @class */ (function (_super) {
    __extends(DirectorsParser, _super);
    function DirectorsParser(logger) {
        var _this = _super.call(this, logger) || this;
        _this.contextKey = 'movie.directors';
        return _this;
    }
    /**
     * Parses directors details from a HTML box and merges them into the given context's `movie` property.
     * @param box
     * @param parsingConfig
     * @param context
     * @returns the merged movie object as in the context after the parsing
     */
    DirectorsParser.prototype.parse = function (box, parsingConfig, context) {
        MethodCallLogger_1.default.logMethodCall();
        context.pushCallstack();
        context.movie = context.movie || {};
        context.movie.directors = this.parseDirectors(box, parsingConfig, context);
        context.popCallstack();
        return context.movie.directors;
    };
    DirectorsParser.prototype.parseDirectors = function (box, parsingConfig, context) {
        var _a;
        MethodCallLogger_1.default.logMethodCall();
        context.pushCallstack();
        var directors = ((_a = context.movie) === null || _a === void 0 ? void 0 : _a.directors) || [];
        if (parsingConfig.directors) {
            var valueGrabber = new ValueGrabber_1.default(parsingConfig.directors, this.logger, this.valueGrabberLogPrefix('directors'));
            var grabbedDirectors = valueGrabber === null || valueGrabber === void 0 ? void 0 : valueGrabber.grab(box, context);
            directors = _.chain([grabbedDirectors])
                .union(directors)
                .flatten()
                .compact()
                .uniq(false, function (director) { return director.toLowerCase(); })
                .value();
        }
        context.popCallstack();
        return directors;
    };
    return DirectorsParser;
}(BaseParser_1.BaseParser));
exports.DirectorsParser = DirectorsParser;
