/// <reference types="cheerio" />
import { Movie } from '../models';
import Logger from '../Logger';
import { ItemParser } from './ItemParser';
import { ParsingContext } from './ParsingContext';
import { VersionParser, VersionParsingConfig } from './VersionParser';
import { ValueGrabbing } from '../ValueGrabber';
import { BaseParser } from './BaseParser';
import { DirectorsParser } from './DirectorsParser';
import { ReleaseDateParser } from './ReleaseDateParser';
import * as moment from 'moment';
/**
 * MovieParsingConifg  with alternative value grabbers when parsing outside a movie context.
 * @category HTML Parsing
 */
export interface IndirectMovieParsingConfig {
    movieTitle?: ValueGrabbing;
    movieTitleOriginal?: ValueGrabbing;
    type?: ValueGrabbing;
}
/**
 * @category HTML Parsing
 */
export interface MovieParsingConfig extends IndirectMovieParsingConfig, VersionParsingConfig {
    id: ValueGrabbing;
    href?: ValueGrabbing;
    title: ValueGrabbing;
    titleOriginal?: ValueGrabbing;
    releaseDate?: ValueGrabbing;
    releaseDateFormat?: moment.MomentFormatSpecification;
    releaseDateLocale?: string;
    directors?: ValueGrabbing;
    imdbId?: ValueGrabbing;
    moviePageLink?: ValueGrabbing;
}
/**
 * @category HTML Parsing
 */
export declare class MovieParser extends BaseParser implements ItemParser<MovieParsingConfig | IndirectMovieParsingConfig> {
    private versionParser;
    private directorsParser;
    private releaseDateParser;
    constructor(logger: Logger, versionParser: VersionParser, directorsParser: DirectorsParser, releaseDateParser: ReleaseDateParser);
    contextKey: string;
    parse(movieBox: cheerio.Cheerio, parsingConfig: MovieParsingConfig, context: ParsingContext): Partial<Movie>;
    private releaseDateParsingConfig;
}
