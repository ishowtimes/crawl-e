import { MovieVersion } from './MovieVersion'

/**
 * @category Model
 */
export interface Movie {
  /** Id of the movie used by the cinema's website */
  id?: string

  /** The movie's localized title as displayed on the cinema's website */
  title: string

  /** The movie's original title as displayed on the cinema's website */
  titleOriginal?: string

  /** The movie's href, relative link or absolute url in case of cinemas publishing showtimes on movie pages */
  href?: string

  /** Map of flags for the movie's version */
  version?: MovieVersion

  /** The movie's directors */
  directors?: string[]

  /** The release date of the movie in ISO8601 format */
  releaseDate?: string,

  /** The type, e.g. "event", "movie", "concert", etc... */
  type?: string,

  /** The movie's imdb id */
  imdbId?: string

  /** The movie's page link */
  moviePageLink?: string
}
