import { expect } from 'chai'
import * as fs from 'fs'
import * as path from 'path'
import * as _ from 'underscore'
import Config from './Config'
import Logger from './Logger'
import Context from './Context'
import { DefaultResponseParser } from './ResponseParsers'
import { TestContext, TestLogger } from './../tests/helpers'
import { Showtime, Movie } from './models';

describe('DefaultResponseParser', () => {

  describe('#handleShowtimesResponse', () => {

    context('movies > dates > showtimes', () => {
      let config
      let testResponse
      let logger: Logger
      let testContext: Context
      let parser: DefaultResponseParser

      beforeEach(() => {
        testResponse = {
          text: fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'release_date_handleShowtimes.html'))
        }
        config = {
          showtimes: {
            movies: {
              box: 'body',
              releaseDate: {
                selector: 'table tr:nth-of-type(2) > td',
                attribute: 'ownText()'
              },
              releaseDateFormat: 'dddd MMMM Do, YYYY',
              releaseDateLocale: 'en',
              title: 'table tr:first-of-type > td:first-of-type',
              dates: {
                box: '.availTimes',
                date: ':box @id',
                dateFormat: 'YYMMDD',
                showtimes: {
                  box: '.showtimeTicket',
                  timeFormat: 'h:mma'
                }
              }
            }
          }
        }
        logger = new TestLogger()
        testContext = new TestContext()
      })

      it('release date crawled from the movie box lands inside the showtimes object', (done) => {
        config = new Config(config)
        parser = new DefaultResponseParser()

        parser.logger = logger
        parser.handleShowtimesResponse(testResponse, config?.showtimes?.[0], testContext, (err, shows: Showtime[] | undefined) => {
          expect(err).to.be.null
          expect(shows).to.be.an.instanceOf(Array)
          expect(shows).to.have.lengthOf(8)
          expect(shows?.[0]).to.deep.include({ release_date: '2023-02-03' })
          done()
        })
      })
    })


    context('showtimes > showtimes', () => {
      let config
      let testResponse
      let logger: Logger
      let testContext: Context
      let parser: DefaultResponseParser

      beforeEach(() => {
        testResponse = {
          text: fs.readFileSync(
            path.join(path.resolve(), 'tests', 'data', 'release_date_showtimes_level.html')
          )
        }
        config = {
          showtimes: {
            showtimes: {
              box: '.heute-slider',
              releaseDate: (box: cheerio.Cheerio) => box.find('.film-content > p:first-of-type').text()?.match(/\d{4}/),
              releaseDateFormat: 'YYYY',
              time: '.film-info-box.content:first-of-type',
              movieTitle: '.film-content h1',
              date: ':box @class',
            }
          }
        }
        logger = new TestLogger()
        testContext = new TestContext()
      })

      it('release date crawled on the showtimes level', (done) => {
        config = new Config(config)
        parser = new DefaultResponseParser()

        parser.logger = logger
        parser.handleShowtimesResponse(testResponse, config?.showtimes?.[0], testContext, (err, shows: Showtime[] | undefined) => {
          expect(err).to.be.null
          expect(shows).to.be.an.instanceOf(Array)
          expect(shows).to.have.length.above(0)
          expect(shows?.[0]).to.deep.include({ release_date: '2022' })
          done()
        })
      })
    })

  })


  describe('#handleMoviesResponse', () => {

    context('movies > list', () => {

      let moviesConfig
      let testResponse
      let logger: Logger
      let testContext: Context
      let parser: DefaultResponseParser

      beforeEach(() => {
        testResponse = {
          text: fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'release_date.html'))
        }
        moviesConfig = {
          movies: {
            list: {
              box: '.grid',
              releaseDate: {
                selector: ':box',
                mapper: (str: string) => str?.match(/\d{4}/)?.[0]
              },
              releaseDateFormat: 'YYYY',
              releaseDateLocale: 'en',
              title: '.text-primary'
            }
          }
        }
        logger = new TestLogger()
        testContext = new TestContext()
      })

      moviesConfig = new Config(moviesConfig)
      parser = new DefaultResponseParser()
      logger = new TestLogger()

      it('release date exists inside movie list', (done) => {
        parser.handleMoviesResponse(testResponse, moviesConfig?.movies?.list, testContext, (err, movies: Movie[] | undefined) => {
          expect(err).to.be.null
          expect(movies?.[1].releaseDate).to.exist
          done()
        })
      })

      let numOfMovies: number = 20
      it(`finds ${numOfMovies} movies with valid release date`, (done) => {
        parser.handleMoviesResponse(testResponse, moviesConfig?.movies?.list, testContext, (err, movies: Movie[] | undefined) => {
          expect(err).to.be.null
          expect(movies).to.have.lengthOf(numOfMovies)
          expect(movies?.map(m => m.releaseDate)).to.have.lengthOf(numOfMovies)
          delete movies?.[1].version
          delete movies?.[1].directors
          delete movies?.[1].type
          expect(movies?.[1]).to.deep.equal({
            title: 'Concerned Citizen',
            releaseDate: '2022'
          })
          done()
        })
      })
    })
  })
})

