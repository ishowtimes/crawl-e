import OutputValidator from './OutputValidator'
import { TestContext } from '../tests/helpers'
import Warnings from './Warnings'
const expect = require('chai').expect



describe('OutputValidator', () => {

  describe('#OutputValidator.validate', () => {

    const testContext = new TestContext()

    it('finds nothing if all is fine (1)', () => {
      var data = {
        crawler: {
          id: 'test',
          is_booking_link_capable: true
        },
        cinema: {
          name: 'Musterkino',
          address: 'Musterstrasse 1, 12345 Musterstadt'
        },
        showtimes: [
          {
            'movie_title': 'Phantastische Tierwesen und wo sie zu finden sind',
            'booking_link': 'https://booking.cineplex.de/#/site/11/performance/37029000023RKBWGMB/mode/sale/',
            'start_at': '2016-12-04T12:30:00'
          },
          {
            'movie_title': 'Fifty Shades of Grey - Gefährliche Liebe',
            'booking_link': 'https://booking.cineplex.de/#/site/11/performance/13029000023RKBWGMB/mode/sale/',
            'start_at': '2017-02-15T19:15:00'
          }
        ]
      }

      expect(OutputValidator.validate(data, testContext)).to.be.empty
    })

    it('finds nothing if all is fine (2)', () => {
      var data = {
        crawler: {
          id: 'test',
          is_booking_link_capable: false
        },
        cinema: {
          name: 'Musterkino',
          address: 'Musterstrasse 1, 12345 Musterstadt'
        },
        showtimes: [
          {
            'movie_title': 'Pettersson und Findus - Das schönste Weihnachten überhaupt',
            'start_at': '2016-12-07T15:00:00',
            'is_3d': false
          },
          {
            'movie_title': "Bridget Jones' Baby",
            'start_at': '2016-12-06T17:00:00',
            'is_3d': false
          }
        ]
      }

      expect(OutputValidator.validate(data, testContext)).to.be.empty
    })

    context('cinema', () => {
      const dummyShowtime = {
        'movie_title': 'Scary Foovie 2',
        'start_at': '2016-12-07T15:00:00',
        'is_3d': false
      }

      it('finds cinema.address missing', () => {
        var data: any = {
          crawler: {
            id: 'test',
            is_booking_link_capable: false
          },
          cinema: {
            name: 'Musterkino',
          },
          showtimes: [dummyShowtime]
        }
        const warnings = OutputValidator.validate(data, testContext)
        expect(warnings).to.have.length(1)
        expect(warnings[0].code).to.equal(3)
      })

      it('finds cinema.slug with underscore', () => {
        var data = {
          crawler: {
            id: 'test',
            is_booking_link_capable: false
          },
          cinema: {
            name: 'Musterkino',
            slug: 'muster_kino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: [dummyShowtime]
        }
        var warnings = OutputValidator.validate(data, testContext)
        expect(warnings).to.have.length(1)
        expect(warnings[0].code).to.equal(8)
      })
    })

    context('showtimes', () => {


      it('finds empty showtimes array', () => {
        var data = {
          crawler: {
            id: 'test',
            is_booking_link_capable: false
          },
          cinema: {
            name: 'Musterkino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: []
        }

        var warnings = OutputValidator.validate(data, testContext)
        expect(warnings).to.have.length(1)
        expect(warnings[0].code).to.equal(7)
      })

      it('skips empty showtimes array if context.isTemporarilyClosed = true', () => {
        let testContext = new TestContext()
        testContext.isTemporarilyClosed = true
        var data = {
          crawler: {
            id: 'test',
            is_booking_link_capable: false
          },
          cinema: {
            name: 'Musterkino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: []
        }

        var warnings = OutputValidator.validate(data, testContext)
        expect(warnings).to.be.empty
      })

      it('finds duplicated booking links', () => {
        var data = {
          crawler: {
            id: 'test',
            is_booking_link_capable: true
          },
          cinema: {
            name: 'Musterkino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: [
            {
              'movie_title': 'Phantastische Tierwesen und wo sie zu finden sind',
              'booking_link': 'https://booking.cineplex.de/#/site/11/performance/37029000023RKBWGMB/mode/sale/',
              'start_at': '2016-12-04T12:30:00'
            },
            {
              'movie_title': 'Fifty Shades of Grey - Gefährliche Liebe',
              'booking_link': 'https://booking.cineplex.de/#/site/11/performance/13029000023RKBWGMB/mode/sale/',
              'start_at': '2017-02-15T19:15:00'
            },
            {
              'movie_title': 'Fifty Shades of Grey - Gefährliche Liebe',
              'booking_link': 'https://booking.cineplex.de/#/site/11/performance/13029000023RKBWGMB/mode/sale/',
              'start_at': '2017-02-16T19:15:00'

            },
            {
              'movie_title': 'Fifty Shades of Grey - Gefährliche Liebe',
              'booking_link': 'https://booking.cineplex.de/#/site/11/performance/13029000023RKBWGMB/mode/sale/',
              'start_at': '2017-02-17T19:15:00'
            }
          ]
        }

        var warnings = OutputValidator.validate(data, testContext)
        expect(warnings).to.have.length(1)
        expect(warnings[0].code).to.equal(1)
        expect(warnings[0].details.bookingLinks.length).to.equal(1)
        expect(warnings[0].details.bookingLinks[0]).to.equal('https://booking.cineplex.de/#/site/11/performance/13029000023RKBWGMB/mode/sale/')
      })

      it('finds invalid booking links', () => {
        var data = {
          crawler: {
            id: 'test',
            is_booking_link_capable: true
          },
          cinema: {
            name: 'Musterkino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: [
            {
              'movie_title': 'Scary Foovie 2',
              'booking_link': 'javascript:void(0);',
              'start_at': '2016-12-04T12:30:00'
            }
          ]
        }

        var warnings = OutputValidator.validate(data, testContext)
        expect(warnings).to.have.length(1)
        expect(warnings[0].code).to.equal(5)
      })

      it('finds all showtimes being at midnight', () => {
        var data = {
          crawler: {
            id: 'test',
            is_booking_link_capable: false
          },
          cinema: {
            name: 'Musterkino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: [
            {
              'movie_title': 'Phantastische Tierwesen und wo sie zu finden sind',
              'start_at': '2016-12-04T00:00:00'
            },
            {
              'movie_title': 'Fifty Shades of Grey - Gefährliche Liebe',
              'start_at': '2017-02-15T00:00:00'
            },
            {
              'movie_title': 'Fifty Shades of Grey - Gefährliche Liebe',
              'start_at': '2017-02-16T00:00:00'

            },
            {
              'movie_title': 'Fifty Shades of Grey - Gefährliche Liebe',
              'start_at': '2017-02-17T00:00:00'
            }
          ]
        }

        var warnings = OutputValidator.validate(data, testContext)
        expect(warnings).to.have.length(1)
        expect(warnings[0].code).to.equal(4)
      })

      it('finds wrong `is_booking_link_capable` (expecting true)', () => {
        var data = {
          crawler: {
            id: 'test',
            is_booking_link_capable: false
          },
          cinema: {
            name: 'Musterkino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: [
            {
              'movie_title': 'Phantastische Tierwesen und wo sie zu finden sind',
              'booking_link': 'https://booking.cineplex.de/#/site/11/performance/37029000023RKBWGMB/mode/sale/',
              'start_at': '2016-12-04T12:30:00'
            },
            {
              'movie_title': 'Fifty Shades of Grey - Gefährliche Liebe',
              'booking_link': 'https://booking.cineplex.de/#/site/11/performance/13029000023RKBWGMB/mode/sale/',
              'start_at': '2017-02-15T19:15:00'
            }
          ]
        }
        var warnings = OutputValidator.validate(data, testContext)
        expect(warnings).to.have.length(1)
        expect(warnings[0].code).to.equal(2)
        expect(warnings[0].recoveryHint).to.contain('is_booking_link_capable: true')
      })

      it('finds wrong `is_booking_link_capable` (expecting false)', () => {
        var data = {
          crawler: {
            id: 'test',
            is_booking_link_capable: true
          },
          cinema: {
            name: 'Musterkino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: [
            {
              'movie_title': 'Phantastische Tierwesen und wo sie zu finden sind',
              'start_at': '2016-12-04T12:30:00'
            },
            {
              'movie_title': 'Fifty Shades of Grey - Gefährliche Liebe',
              'start_at': '2017-02-15T19:15:00'
            }
          ]
        }
        var warnings = OutputValidator.validate(data, testContext)
        expect(warnings).to.have.length(1)
        expect(warnings[0].code).to.equal(2)
        expect(warnings[0].recoveryHint).to.contain('is_booking_link_capable: false')
      })

      context('release_date validation', () => {
        const data = (releaseDate: string) => ({
          crawler: {
            id: 'test',
            is_booking_link_capable: false
          },
          cinema: {
            name: 'KINO',
            address: 'Street 1, 12345 City'
          },
          showtimes: [
            {
              'movie_title': 'KNOCK AT THE CABIN',
              'release_date': releaseDate,
              'start_at': '2023-02-06T19:30:00',
              'is_3d': false
            },
            {
              'movie_title': 'KNOCK AT THE CABIN',
              'start_at': '2023-02-06T20:30:00',
              'is_3d': false
            }
          ]
        })

        let validReleaseDates: any[] = [
          "2023",
          "2023-02",
          "2023-02-03",
          undefined,
          null
        ]
        validReleaseDates.forEach(releaseDate => {
          it(`finds valid release date ${releaseDate}`, () => {
            const warnings: Warnings.Warning[] = OutputValidator.validate(data(releaseDate), testContext)
            expect(warnings).to.be.empty
          })
        })

        let invalidReleaseDates: string[] = [
          "2023-02-03T",
          "2023-02-03T21:00",
          "20233",
          "2023-00",
          "2023-02-32"
        ]
        invalidReleaseDates.forEach(invalidReleaseDate => {
          it(`finds invalid release date (${invalidReleaseDate})`, () => {
            const warnings: Warnings.Warning[] = OutputValidator.validate(data(invalidReleaseDate), testContext)
            expect(warnings).to.have.length(2)
            expect(warnings[0].code).to.equal(5)
            expect(warnings[1].code).to.equal(9)
            expect(warnings[1].title).to.equal('The release date is invalid or contains time')
          })
        })


      }) // context releaseDate validation

      context('language validation', () => {
        let createOutputData = (language) => ({
          crawler: {
            id: 'test',
            is_booking_link_capable: false
          },
          cinema: {
            name: 'Musterkino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: [
            {
              movie_title: 'Phantastische Tierwesen und wo sie zu finden sind',
              start_at: '2016-12-04T12:30:00',
              language: language
            }
          ]
        })

        let testIsoCodes = ['en', 'de', 'de-DE', 'zh-Hans', 'zh-Hans-HK']
        testIsoCodes.forEach(testCode => {
          it(`passes on valid ISO 639 codes (${testCode})`, () => {
            let data = createOutputData(testCode)
            let warnings = OutputValidator.validate(data, testContext)
            expect(warnings).to.be.empty
          })
        })

        it(`passes on 'original version'`, () => {
          let data = createOutputData('original version')
          let warnings = OutputValidator.validate(data, testContext)
          expect(warnings).to.be.empty
        })

        let invalidLanguageExamples = ['deutsch', 'OmU', 'english original']
        invalidLanguageExamples.forEach(example => {
          it(`finds invalid values (${example})`, () => {
            let data = createOutputData(example)
            let warnings = OutputValidator.validate(data, testContext)
            expect(warnings).to.have.length(1)
            expect(warnings[0].code).to.equal(5) // expect validation error against output schema due to regex on language
          })
        })

      }) // context language validation


      context('subtitles validation', () => {
        let createOutputData = (subtitles) => ({
          crawler: {
            id: 'test',
            is_booking_link_capable: false
          },
          cinema: {
            name: 'Musterkino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: [
            {
              movie_title: 'Phantastische Tierwesen und wo sie zu finden sind',
              start_at: '2016-12-04T12:30:00',
              subtitles: subtitles
            }
          ]
        })

        let validExamples = ['en', 'de', 'de-DE', 'zh-Hans', 'zh-Hans-HK', 'de,fr', 'de-CH,fr-CH']
        validExamples.forEach(example => {
          it(`passes on valid ISO 639 codes (${example})`, () => {
            let data = createOutputData(example)
            let warnings = OutputValidator.validate(data, testContext)
            expect(warnings).to.be.empty
          })
        })

        it(`passes on 'undetermined'`, () => {
          let data = createOutputData('undetermined')
          let warnings = OutputValidator.validate(data, testContext)
          expect(warnings).to.be.empty
        })

        let invalidLanguageExamples = ['deutsch', 'OmU', 'english original', 'mit Untertitel', 'de fr']
        invalidLanguageExamples.forEach(example => {
          it(`finds invalid values (${example})`, () => {
            var data = createOutputData(example)
            var warnings = OutputValidator.validate(data, testContext)
            expect(warnings).to.have.length(1)
            expect(warnings[0].code).to.equal(5) // expect validation error against output schema due to regex on language
          })
        })

      }) // context subtitles validation

      context('directors validation', () => {

        let createOutput = (directors: any[]) => ({
          crawler: {
            id: 'test',
            is_booking_link_capable: true
          },
          cinema: {
            name: 'Musterkino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: [
            {
              'movie_title': 'Phantastische Tierwesen und wo sie zu finden sind',
              'booking_link': 'https://booking.cineplex.de/#/site/11/performance/37029000023RKBWGMB/mode/sale/',
              'start_at': '2016-12-04T12:30:00',
              'directors': directors
            }
          ]
        })

        let invalidDirectorsValues = [['Peter Jackson', null, {}, [], 'Christopher Nolan'], [null], [{}], [[]], [1], [true], [false], [undefined], [NaN], [Infinity], [-Infinity]]
        let validDirectorValues = [['Peter Jackson'], ['Christopher Nolan', 'Jonathan Nolan']]

        invalidDirectorsValues.forEach(directors => {
          it(`finds INVALID directors array [${directors}]`, () => {
            let data = createOutput(directors)
            let warnings: Warnings.Warning[] = OutputValidator.validate(data, testContext)
            expect(warnings).to.have.length(1)
            expect(warnings[0].code).to.equal(5)
          })
        })

        validDirectorValues.forEach(directors => {
          it(`passes on VALID directors array [${directors}]`, () => {
            let data = createOutput(directors)
            let warnings: Warnings.Warning[] = OutputValidator.validate(data, testContext)
            expect(warnings).to.have.length(0)
          })
        })

      }) // context directors validation

      context('type validation', () => {
        let createOutput = (type: any) => ({
          crawler: {
            id: 'test',
            is_booking_link_capable: false
          },
          cinema: {
            name: 'Musterkino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: [
            {
              'movie_title': 'Phantastische Tierwesen und wo sie zu finden sind',
              'start_at': '2016-12-04T12:30:00',
              'type': type
            },
            {
              'movie_title': 'Phantastische Tierwesen und wo sie zu finden sind',
              'start_at': '2016-12-04T12:30:00'
            }
          ]
        })

        let invalidTypeValues = [{}, [], 1, true, false, NaN, Infinity, -Infinity]
        let validTypeValues = ['event', 'ROH', 'movie', 'concert', null, undefined]

        invalidTypeValues.forEach(type => {
          it(`finds INVALID type ${type}`, () => {
            let data = createOutput(type) 
            let warnings: Warnings.Warning[] = OutputValidator.validate(data, testContext)
            expect(warnings).to.have.length(1)
            expect(warnings[0].code).to.equal(5)
          })
        })

        validTypeValues.forEach(type => {
          it(`passes on VALID type ${type}`, () => {
            let data = createOutput(type)
            let warnings: Warnings.Warning[] = OutputValidator.validate(data, testContext)
            expect(warnings).to.have.length(0)
          })
        })

      }) // context type validation

      context('movie_imdb_id validation', () => {
        let createOutput = (imdbId: any) => ({
          crawler: {
            id: 'test',
            is_booking_link_capable: false
          },
          cinema: {
            name: 'Musterkino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: [
            {
              'movie_title': 'Phantastische Tierwesen und wo sie zu finden sind',
              'start_at': '2016-12-04T12:30:00',
              'movie_imdb_id': imdbId
            },
            {
              'movie_title': 'Phantastische Tierwesen und wo sie zu finden sind',
              'start_at': '2016-12-04T12:30:00'
            }
          ]
        })

        let invalidImdbIdValues = [{}, [], 1, true, false, NaN, Infinity, -Infinity]
        let validImdbIdValues = ['tt000000', 'tt1234567', null, undefined]

        invalidImdbIdValues.forEach(imdbId => {
          it(`finds INVALID movie_imdb_id ${imdbId}`, () => {
            let data = createOutput(imdbId) 
            let warnings: Warnings.Warning[] = OutputValidator.validate(data, testContext)
            expect(warnings).to.have.length(1)
            expect(warnings[0].code).to.equal(5)
          })
        })

        validImdbIdValues.forEach(imdbId => {
          it(`passes on VALID movie_imdb_id ${imdbId}`, () => {
            let data = createOutput(imdbId)
            let warnings: Warnings.Warning[] = OutputValidator.validate(data, testContext)
            expect(warnings).to.have.length(0)
          })
        })

      }) // context movie_imdb_id validation

      context('movie_page_link validation', () => {
        let createOutput = (moviePageLink: any) => ({
          crawler: {
            id: 'test',
            is_booking_link_capable: false
          },
          cinema: {
            name: 'Musterkino',
            address: 'Musterstrasse 1, 12345 Musterstadt'
          },
          showtimes: [
            {
              'movie_title': 'Phantastische Tierwesen und wo sie zu finden sind',
              'start_at': '2016-12-04T12:30:00',
              'movie_page_link': moviePageLink
            },
            {
              'movie_title': 'Phantastische Tierwesen und wo sie zu finden sind',
              'start_at': '2016-12-04T12:30:00'
            }
          ]
        })

        let invalidMoviePageLinks = ['tt000000', 'tt1234567', '//http:example.com', 'http//www.example.com', {}, '[]', 1, true, false, NaN, Infinity, -Infinity]
        let validMoviePageLinks = [
          'http://192.192.1.2:3001/movies?q=%22Avatar%22',
          'http://example.com/movies?item=123', 
          'example.com/item=1234', 
          'www.example.com?movies=movie1', 
          'foo.website.gov.uk/movie1', 
          null, 
          undefined
        ]

        invalidMoviePageLinks.forEach(moviePageLink => {
          it(`finds INVALID movie_page_link ${moviePageLink}`, () => {
            let data = createOutput(moviePageLink) 
            let warnings: Warnings.Warning[] = OutputValidator.validate(data, testContext)
            expect(warnings).to.have.length(1)
            expect(warnings[0].code).to.equal(5)
          })
        })

        validMoviePageLinks.forEach(moviePageLink => {
          it(`passes on VALID movie_page_link ${moviePageLink}`, () => {
            let data = createOutput(moviePageLink)
            let warnings: Warnings.Warning[] = OutputValidator.validate(data, testContext)
            expect(warnings).to.have.length(0)
          })
        })

      }) // context movie_page_link validation
    })

  })
})
