import { expect } from 'chai'
import * as fs from 'fs'
import * as path from 'path'
import * as cheerio from 'cheerio'
import Context from '../Context'
import Config from '../Config'
import { Logger, SilentLogger } from '../Logger'
import { TestContext, TestLogger } from '../../tests/helpers'
import { DefaultResponseParser } from '../ResponseParsers'
import { MoviePageLinkParser, MoviePageLinkParsingConfig } from './MoviePageLinkParser'
import { Showtime } from '../models'
import { ValueGrabbing } from '../ValueGrabber'


describe('MoviePageLinkParser', () => {
  const parser = new MoviePageLinkParser(new SilentLogger())

  describe('#parse', () => {

    let testContext: Context
    let $: cheerio.Root
    let boxes: cheerio.Cheerio

    beforeEach(() => {
      const testHtml: any = fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'showtimes_everything_in_box.html'))
      $ = cheerio.load(testHtml)
      boxes = $('.showtime')
    })

    context('moviePageLink', () => {

      const parsemoviePageLink = (box: cheerio.Cheerio, givenContext: any = {}) => {
        testContext = new TestContext({ cheerio: $, ...givenContext })

        const moviePageLink: ValueGrabbing = {
          selector: 'h2.title a',
          attribute: 'href',
          mapper: (value: any) => value.trim()
        }

        const moviePageLinkConfig: MoviePageLinkParsingConfig = {
          moviePageLink
        }
        parser.parse(box, moviePageLinkConfig, testContext)
      }
      
      let firstMoviePageLink: string = 'http://mycinema.com/movies?item=4711'
      it(`parses movie page link ${firstMoviePageLink}`, () => {
        const box = boxes.eq(0)
        parsemoviePageLink(box, testContext)
        expect(testContext?.movie?.moviePageLink).to.equal(firstMoviePageLink)
      })

      let secondMoviePageLink: string = 'http://mycinema.com/movies?item=4712'
      it(`parses movie page link ${secondMoviePageLink}`, () => {
        const box = boxes.eq(1)
        parsemoviePageLink(box, testContext)
        expect(testContext?.movie?.moviePageLink).to.equal(secondMoviePageLink)
      })

    })
  })
}) // MoviePageLinkParser


describe('DefaultResponseParser', () => {
  let logger: Logger
  let testContext: Context
  let parser: DefaultResponseParser

  describe('#handleShowtimesResponse', () => {

    beforeEach(() => {
      logger = new TestLogger()
      testContext = new TestContext()
      parser = new DefaultResponseParser()
      parser.logger = logger
    })
     
    context('movies > showtimes', () => {
      const testResponse: any = {
        text: fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'showtimes_by_movies.html'))
      }
      const config1 = {
        showtimes: {
          movies: {
            box: '.movie',
            title: '.title',
            moviePageLink: (box: cheerio.Cheerio) => box.attr('href')?.trim(),
            showtimes: {
              box: 'li.showtime > a.showtime',
              datetime: ':box',
              datetimeFormat: ['dd DD.MM.YYYY - HH:mm', 'DD.MM.YYYY - HH:mm'],
              datetimeLocale: 'de'
            }
          }
        }
      }

      const moviePageLink = 'http://mycinema.com/movies?item=4710'
      it(`passes for movie_page_link: ${moviePageLink} crawled on movies level`, (done) => {
        const config = new Config(config1)
        parser.handleShowtimesResponse(testResponse, config?.showtimes?.[0], testContext, (err, shows: Showtime[] | undefined) => {
          expect(err).to.be.null
          expect(shows?.[0].movie_page_link).to.equal(moviePageLink)
          done()
        })
      })
      
    }) // parsed 'movie_page_link' on config: movies > showtimes


    context('showtimes > showtimes', () => {
      const testResponse: any = {
        text: fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'showtimes_everything_in_box.html'))
      }
      const config2 = {
        showtimes: {
          showtimes: {
            box: 'li.showtime',
            movieTitle: '.title',
            moviePageLink: {
              selector: '.title a',
              attribute: 'href',
              mapper: (value: string) => value.trim()
            },
            datetime: 'time[datetime] @datetime',
            datetimeFormat: 'YYYY-MM-DD HH:mm:ss'
          }
        }
      }
      
      it('passes for movie_page_link crawled on showtimes level', (done) => {
        const config = new Config(config2)
        parser.handleShowtimesResponse(testResponse, config?.showtimes?.[0], testContext, (err, shows: Showtime[] | undefined) => {
          expect(err).to.be.null
          expect(shows?.[0].movie_page_link).to.exist.and.equal('http://mycinema.com/movies?item=4711')
          expect(shows?.[1].movie_page_link).to.exist.and.equal('http://mycinema.com/movies?item=4712')
          done()
        })
      })

    }) // parsed 'movie_page_link' on config: showtimes > showtimes

  }) // #handleShowtimesResponse

}) // DefaultResponseParser