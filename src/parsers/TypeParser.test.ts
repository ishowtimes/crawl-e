import { expect } from 'chai'
import * as fs from 'fs'
import * as path from 'path'
import * as cheerio from 'cheerio'
import Context from '../Context'
import Config from '../Config'
import { Logger, SilentLogger } from '../Logger'
import { TestContext, TestLogger } from '../../tests/helpers'
import { DefaultResponseParser } from '../ResponseParsers'
import { TypeParser, TypeParsingConfig } from './TypeParser'
import { Showtime } from '../models'
import { ValueGrabbing } from '../ValueGrabber'


describe('TypeParser', () => {
  const parser = new TypeParser(new SilentLogger())

  describe('#parse', () => {

    let testContext: Context
    let $: cheerio.Root
    let boxes: cheerio.Cheerio

    beforeEach(() => {
      let testHtml: any = fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'showtimes_everything_in_box.html'))
      $ = cheerio.load(testHtml)
      boxes = $('.showtime')
    })

    context('type', () => {

      const parseType = (box: cheerio.Cheerio, givenContext: any = {}) => {
        testContext = new TestContext({ cheerio: $, ...givenContext })

        let type: ValueGrabbing = {
          selector: '.type',
          mapper: (value: any) => value.trim()
        }

        let typeConfig: TypeParsingConfig = {
          type
        }
        parser.parse(box, typeConfig, testContext)
      }

      it('parses event type', () => {
        const box = boxes.eq(0)
        parseType(box, testContext)
        expect(testContext?.movie?.type).to.equal('event')
      })

      it('parses movie type', () => {
        const box = boxes.eq(1)
        parseType(box, testContext)
        expect(testContext?.movie?.type).to.equal('movie')
      })

    })
  })
}) // TypeParser


describe('DefaultResponseParser', () => {

  let logger: Logger
  let testContext: Context
  let parser: DefaultResponseParser
  
  describe('#handleShowtimesResponse', () => {
    
    beforeEach(() => {
      logger = new TestLogger()
      testContext = new TestContext()
      parser = new DefaultResponseParser()
      parser.logger = logger
    })
     
    context('movies > showtimes', () => {

      const testResponse: any = {
        text: fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'showtimes_by_movies.html'))
      }
      const config1 = {
        showtimes: {
          movies: {
            box: '.movie',
            title: '.title',
            type: (box: cheerio.Cheerio) => box.find('.type').text().trim(),
            showtimes: {
              box: 'li.showtime > a.showtime',
              datetime: ':box',
              datetimeFormat: ['dd DD.MM.YYYY - HH:mm', 'DD.MM.YYYY - HH:mm'],
              datetimeLocale: 'de'
            }
          }
        }
      }

      it('passes for type crawled on movies level', (done) => {
        const config = new Config(config1)
        parser.handleShowtimesResponse(testResponse, config?.showtimes?.[0], testContext, (err, shows: Showtime[] | undefined) => {
          expect(err).to.be.null
          expect(shows?.[0].type).to.exist.and.equal('anniversary')
          done()
        })
      })

    }) // parsed 'type' on config: movies > showtimes


    context('showtimes > showtimes', () => {

      const testResponse: any = {
        text: fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'showtimes_everything_in_box.html'))
      }  
      const config2 = {
        showtimes: {
          showtimes: {
            box: 'li.showtime',
            movieTitle: '.title',
            type: {
              selector: '.type',
              mapper: (value: string) => value.trim()
            },
            datetime: 'time[datetime] @datetime',
            datetimeFormat: 'YYYY-MM-DD HH:mm:ss'
          }
        }
      }
      
      it('passes for type crawled on showtimes level', (done) => {
        const config = new Config(config2)        
        parser.handleShowtimesResponse(testResponse, config?.showtimes?.[0], testContext, (err, shows: Showtime[] | undefined) => {
          expect(err).to.be.null
          expect(shows?.[0].type).to.exist.and.equal('event')
          expect(shows?.[1].type).to.exist.and.equal('movie')
          done()
        })
      })

    }) // parsed 'type' on config: showtimes > showtimes

  }) // #handleShowtimesResponse

}) // DefaultResponseParser