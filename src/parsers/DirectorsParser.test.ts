import { expect } from 'chai'
import * as fs from 'fs'
import * as path from 'path'
import * as cheerio from 'cheerio'
import Context from '../Context'
import Config from '../Config'
import { Logger, SilentLogger } from '../Logger'
import { TestContext, TestLogger } from '../../tests/helpers'
import { DefaultResponseParser } from '../ResponseParsers'
import { DirectorsParser, DirectorsParsingConfig } from './DirectorsParser';
import { Showtime } from '../models';
import { ValueGrabbing } from '../ValueGrabber'


describe('DirectorsParser', () => {
  const parser = new DirectorsParser(new SilentLogger())

  describe('#parse', () => {

    let testContext: Context
    let $: cheerio.Root
    let boxes: cheerio.Cheerio

    beforeEach(() => {
      let testHtml: any = fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'showtimes_everything_in_box.html'))
      $ = cheerio.load(testHtml)
      boxes = $('.showtime')
    })

    context('directors', () => {

      const parseDirectors = (box: cheerio.Cheerio, givenContext: any = {}) => {
        testContext = new TestContext({ cheerio: $, ...givenContext })

        let directors: ValueGrabbing = {
          selector: '.directors',
          mapper: (value: any) => {
            return value.split(/,\s*/g)
          }
        }

        let directorConfig: DirectorsParsingConfig = {
          directors
        }
        parser.parse(box, directorConfig, testContext)
      }

      beforeEach(() => {
        parseDirectors(boxes.first())
      })

      it('finds directors', () => {
        expect(testContext?.movie?.directors).length.to.be.greaterThan(0)
      })
    })

  })

})

describe('DefaultResponseParser', () => {

  describe('#handleShowtimesResponse', () => {

    context('movies > showtimes', () => {
      let config: any
      let testResponse: any
      let logger: Logger
      let testContext: Context
      let parser: DefaultResponseParser

      beforeEach(() => {
        testResponse = {
          text: fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'showtimes_by_movies.html'))
        }
        config = {
          showtimes: {
            movies: {
              box: '.movie',
              title: '.title',
              directors: {
                selector: '.directors',
                mapper: (value: string) => value.split(/,\s*/g)
              },
              showtimes: {
                box: 'li.showtime > a.showtime',
                datetime: ':box',
                datetimeFormat: ['dd DD.MM.YYYY - HH:mm', 'DD.MM.YYYY - HH:mm'],
                datetimeLocale: 'de'
              }
            }
          }
        }
        testContext = new TestContext()
      })


      it('passes for directors crawled on movies level', (done) => {
        logger = new TestLogger()
        config = new Config(config)
        parser = new DefaultResponseParser()
        parser.logger = logger

        parser.handleShowtimesResponse(testResponse, config?.showtimes?.[0], testContext, (err, shows: Showtime[] | undefined) => {
          expect(err).to.be.null
          expect(shows?.[0].directors).to.exist
          done()
        })
      })
    })

    context('showtimes > showtimes', () => {
      let config: any
      let testResponse: any
      let logger: Logger
      let testContext: Context
      let parser: DefaultResponseParser

      beforeEach(() => {
        testResponse = {
          text: fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'showtimes_everything_in_box.html'))
        }
        config = {
          showtimes: {
            showtimes: {
              box: 'li.showtime',
              movieTitle: '.title',
              directors: {
                selector: '.directors',
                mapper: (value: string) => value.split(/,\s*/g)
              },
              datetime: 'time[datetime] @datetime',
              datetimeFormat: 'YYYY-MM-DD HH:mm:ss'
            }
          }
        }
        testContext = new TestContext()
      })


      it('passes for directors on showtimes level', (done) => {
        logger = new TestLogger()
        config = new Config(config)
        parser = new DefaultResponseParser()
        parser.logger = logger

        parser.handleShowtimesResponse(testResponse, config?.showtimes?.[0], testContext, (err, shows: Showtime[] | undefined) => {
          expect(err).to.be.null
          expect(shows?.[0].directors).to.have.lengthOf(2)
          expect(shows?.[1].directors).to.have.lengthOf(1)
          done()
        })
      })
    })
  })

})