import { ParsingContext } from './ParsingContext'
import MethodCallLogger from '../MethodCallLogger'
import { ItemParser } from './ItemParser'
import { ValueGrabbing } from '../ValueGrabber'
import { BaseParser } from './BaseParser'

export interface ImdbIdParsingConfig {
  /**
   * Value Grabber for the imdb id.
   */
  imdbId?: ValueGrabbing
}

/**
 * ImdbIdParser
 * @category HTML Parsing
 */
export class ImdbIdParser extends BaseParser implements ItemParser<ImdbIdParsingConfig> {
  contextKey = 'imdbId'

  /**
   * Parses a single imdb id from a HTML box.
   * @param imdbIdBox the HTML box to grab the imdb id string from
   * @param imdbIdParsingConfig config for grabbing the imdb id string and parsing it
   * @param context the context which to set the `imdb id` property on
   * @returns the parsed imdb id as string
   */
  parse(imdbIdBox: cheerio.Cheerio, imdbIdParsingConfig: ImdbIdParsingConfig, context: ParsingContext): string {
    MethodCallLogger.logMethodCall()
    context.pushCallstack()
    context.movie = context.movie || {}
    context.movie.imdbId = this.resovleValueGrabber('imdbId', imdbIdParsingConfig).grabFirst(imdbIdBox, context)
    context.popCallstack()
    return context.movie.imdbId
  }
}