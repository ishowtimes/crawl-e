import { ParsingContext } from './ParsingContext'
import { ItemParser } from './ItemParser'
import ValueGrabber, { ValueGrabbing } from '../ValueGrabber'
import { BaseParser } from './BaseParser'
import MethodCallLogger from '../MethodCallLogger'

/**
 * @category HTML Parsing
 */
export interface TypeParsingConfig {
  type?: ValueGrabbing
}

/**
 * Parser for `type`
 * @category HTML Parsing
 */
export class TypeParser extends BaseParser implements ItemParser<TypeParsingConfig> {
  contextKey = 'type'

  /**
   * Parses a HTML box and type into the given context's property.
   * @param box
   * @param config
   * @param context the context which to set the type details on
   * @returns the parsed type details
   */
  parse(box: cheerio.Cheerio, parsingConfig: TypeParsingConfig, context: ParsingContext): string {
    MethodCallLogger.logMethodCall()
    
    context.pushCallstack()
    context.movie = context.movie || {}
    context.movie.type = this.parseType(box, parsingConfig, context)
    context.popCallstack()

    return context.movie.type
  }

  parseType(box: cheerio.Cheerio, parsingConfig: TypeParsingConfig, context: ParsingContext): string {
    MethodCallLogger.logMethodCall()
    
    context.pushCallstack()
    let type: string = context.movie?.type
    if (parsingConfig.type) {
      const valueGrabber = new ValueGrabber(parsingConfig.type, this.logger, this.valueGrabberLogPrefix('type'))
      type = valueGrabber.grabFirst(box, context)
    }
    context.popCallstack()

    return type
  }

}