import * as moment from 'moment'
import * as colors from 'colors'
import * as _ from 'underscore'

import Constants from '../Constants'
import { ParsingContext } from './ParsingContext'
import MethodCallLogger from '../MethodCallLogger'
import { ItemParser } from './ItemParser'
import { ValueGrabbing } from '../ValueGrabber'
import { BaseParser } from './BaseParser';

/** @private */
export const styleStr = (str: any) => str
  ? colors.underline.green(str)
  : colors.inverse.red('null')

/** @private */
export const styleMom = (m: moment.Moment, format: string) => {
  let color = m.isValid() ? colors.green : colors.red
  return colors.bold(color(m.format(format)))
}

export type ReleaseDate = string

/**
 * @category HTML Parsing
 */
export interface ReleaseDateStringParsingConfig {
  /**
   * Format(s) for parsing a release date string selected by the `release date` ValueGrabber
  */
  releaseDateFormat?: moment.MomentFormatSpecification,
  /**
   * Locale for parsing a release date string. (Specified as [ISO 639 code](http://www.localeplanet.com/icu/iso639.html))
  */
  releaseDateLocale?: string
  /**
   * Disables year correction of release date without explicit year information."
   * @default false
  */
  preserveYear?: boolean
}

/**
 * @category HTML Parsing
 */
export interface ReleaseDateParsingConfig extends ReleaseDateStringParsingConfig {
  /**
   * Value Grabber for the release date string.
   */
  releaseDate?: ValueGrabbing
}

/**
 * ReleaseDateParser
 * @category HTML Parsing
 */
export class ReleaseDateParser extends BaseParser implements ItemParser<ReleaseDateParsingConfig> {
  debugKey = 'selection:releaseDate:parsing'
  contextKey = 'releaseDate'
  logPrefix = 'releaseDate'

  /**
   * Parses a single release date from a HTML box.
   * @param releaseDateBox the HTML box to grab the release date string from
   * @param releaseDateParsingConfig config for grabbing the release date string and parsing it
   * @param context the context which to set the `release date` property on
   * @returns the parsed release date as moment object
   */
  parse(releaseDateBox: cheerio.Cheerio, releaseDateParsingConfig: ReleaseDateParsingConfig, context: ParsingContext) {
    MethodCallLogger.logMethodCall()
    context.pushCallstack()
    let releaseDateStr = this.resovleValueGrabber('releaseDate', releaseDateParsingConfig).grabFirst(releaseDateBox, context)
    let releaseDate = this.parseReleaseDateStr(releaseDateStr, releaseDateParsingConfig, context)
    context.popCallstack()
    return releaseDate
  }

  /**
   * Parses a release date string.
   * @param releaseDateStr the formatted release date string to parse
   * @param releaseDateParsingConfig config for parsing the release date string
   * @param context the context which to set the `release date` property on
   * @returns the parsed release date as moment object
   */
  parseReleaseDateStr(releaseDateStr: string, releaseDateParsingConfig: ReleaseDateStringParsingConfig, context: ParsingContext) {
    MethodCallLogger.logMethodCall()
    context.pushCallstack()
    let releaseDate: moment.Moment
    function testMatchAnyWords(releaseDateStr: string | undefined, words: string[]) {
      if (typeof releaseDateStr !== 'string') { return false }
      if (words.indexOf(releaseDateStr.trim()) !== -1) { return true }
      let regex = new RegExp(
        '\\b(' + words
          .map(w => `(${w})`)
          .join('|') + ')\\b',
        'gi'
      )
      return regex.test(releaseDateStr.trim())
    }

    releaseDate = moment(releaseDateStr, releaseDateParsingConfig.releaseDateFormat, releaseDateParsingConfig.releaseDateLocale, true)

    if (!releaseDate.isValid() && testMatchAnyWords(releaseDateStr, Constants.DAY_AFTER_TOMORROW_WORS)) {
      releaseDate = moment().add(2, 'days').set('hour', 0)
      this.logger.debug(this.debugKey, 'releaseDateStr:', styleStr(releaseDateStr), '-> matched regex for day after tomorrow', colors.bold(' => '), styleMom(releaseDate, 'YYYY-MM-DD'))
    } else if (!releaseDate.isValid() && testMatchAnyWords(releaseDateStr, Constants.TOMORROW_WORS)) {
      releaseDate = moment().add(1, 'day').set('hour', 0)
      this.logger.debug(this.debugKey, 'releaseDateStr:', styleStr(releaseDateStr), '-> matched tomorrow regex', colors.bold(' => '), styleMom(releaseDate, 'YYYY-MM-DD'))
    } else if (!releaseDate.isValid() && testMatchAnyWords(releaseDateStr, Constants.TODAY_WORDS)) {
      releaseDate = moment().set('hour', 0)
      this.logger.debug(this.debugKey, 'releaseDateStr:', styleStr(releaseDateStr), '-> matched today regex', colors.bold(' => '), styleMom(releaseDate, 'YYYY-MM-DD'))
    } else {
      this.logger.debug(this.debugKey, 'releaseDateStr:', styleStr(releaseDateStr), 'format:', styleStr(releaseDateParsingConfig.releaseDateFormat), 'locale:', styleStr(releaseDateParsingConfig.releaseDateLocale), colors.bold(' => '), styleMom(releaseDate, 'YYYY-MM-DD'))
    }
    context.popCallstack()
    return releaseDate
  }
}