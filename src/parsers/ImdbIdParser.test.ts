import { expect } from 'chai'
import * as fs from 'fs'
import * as path from 'path'
import * as cheerio from 'cheerio'
import Context from '../Context'
import Config from '../Config'
import { Logger, SilentLogger } from '../Logger'
import { TestContext, TestLogger } from '../../tests/helpers'
import { DefaultResponseParser } from '../ResponseParsers'
import { ImdbIdParser, ImdbIdParsingConfig } from './ImdbIdParser'
import { Showtime } from '../models'
import { ValueGrabbing } from '../ValueGrabber'


describe('ImdbIdParser', () => {
  const parser = new ImdbIdParser(new SilentLogger())

  describe('#parse', () => {

    let testContext: Context
    let $: cheerio.Root
    let boxes: cheerio.Cheerio

    beforeEach(() => {
      const testHtml: any = fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'showtimes_everything_in_box.html'))
      $ = cheerio.load(testHtml)
      boxes = $('.showtime')
    })

    context('imdbId', () => {

      const parseImdbId = (box: cheerio.Cheerio, givenContext: any = {}) => {
        testContext = new TestContext({ cheerio: $, ...givenContext })

        const imdbId: ValueGrabbing = {
          selector: '.imdbID',
          mapper: (value: any) => value.trim()
        }

        const imdbIdConfig: ImdbIdParsingConfig = {
          imdbId
        }
        parser.parse(box, imdbIdConfig, testContext)
      }
      
      it('parses imdb id tt0257106', () => {
        const box = boxes.eq(0)
        parseImdbId(box, testContext)
        expect(testContext?.movie?.imdbId).to.equal('tt0257106')
      })

      it('parses imdb id tt0121765', () => {
        const box = boxes.eq(1)
        parseImdbId(box, testContext)
        expect(testContext?.movie?.imdbId).to.equal('tt0121765')
      })

    })
  })
}) // ImdbIdParser


describe('DefaultResponseParser', () => {
  
  let logger: Logger
  let testContext: Context
  let parser: DefaultResponseParser

  describe('#handleShowtimesResponse', () => {

    beforeEach(() => {
      logger = new TestLogger()
      testContext = new TestContext()
      parser = new DefaultResponseParser()
      parser.logger = logger
    })

    context('movies > showtimes', () => {

      const testResponse: any = {
        text: fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'showtimes_by_movies.html'))
      }
      const config1 = {
        showtimes: {
          movies: {
            box: '.movie',
            title: '.title',
            imdbId: (box: cheerio.Cheerio) => box.find('.imdbID').text().trim(),
            showtimes: {
              box: 'li.showtime > a.showtime',
              datetime: ':box',
              datetimeFormat: ['dd DD.MM.YYYY - HH:mm', 'DD.MM.YYYY - HH:mm'],
              datetimeLocale: 'de'
            }
          }
        }
      }
      let imdbId = 'tt0257100'
      it(`passes for movie_imdb_id: ${imdbId} crawled on movies level`, (done) => {
        const config = new Config(config1)
        parser.handleShowtimesResponse(testResponse, config?.showtimes?.[0], testContext, (err, shows: Showtime[] | undefined) => {
          expect(err).to.be.null
          expect(shows?.[0].movie_imdb_id).to.exist.and.equal(imdbId)
          done()
        })
      })

    }) // parsed 'movie_imdb_id' on config: movies > showtimes


    context('showtimes > showtimes', () => {

      const testResponse: any = {
        text: fs.readFileSync(path.join(path.resolve(), 'tests', 'data', 'showtimes_everything_in_box.html'))
      }
      const config2 = {
        showtimes: {
          showtimes: {
            box: 'li.showtime',
            movieTitle: '.title',
            imdbId: {
              selector: '.imdbID',
              mapper: (value: string) => value.trim()
            },
            datetime: 'time[datetime] @datetime',
            datetimeFormat: 'YYYY-MM-DD HH:mm:ss'
          }
        }
      }
      
      it('passes for movie_imdb_id crawled on showtimes level', (done) => {
        const config = new Config(config2)
        parser.handleShowtimesResponse(testResponse, config?.showtimes?.[0], testContext, (err, shows: Showtime[] | undefined) => {
          expect(err).to.be.null
          expect(shows?.[0].movie_imdb_id).to.exist.and.equal('tt0257106')
          expect(shows?.[1].movie_imdb_id).to.exist.and.equal('tt0121765')
          done()
        })
      })

    }) // parsed 'movie_imdb_id' on config: showtimes > showtimes

  }) // #handleShowtimesResponse

}) // DefaultResponseParser