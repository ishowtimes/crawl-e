import * as _ from 'underscore'
import Logger from '../Logger'
import MethodCallLogger from '../MethodCallLogger'
import { ItemParser } from './ItemParser'
import { ParsingContext } from './ParsingContext'
import ValueGrabber, { ValueGrabbing } from '../ValueGrabber';
import { BaseParser } from './BaseParser';

/**
 * DirectorsParsingConfig
 * @category HTML Parsing
 */
export interface DirectorsParsingConfig {
  directors?: ValueGrabbing
}

/**
 * Parses directors`. 
 * @category HTML Parsing
 */
export class DirectorsParser extends BaseParser implements ItemParser<DirectorsParsingConfig> {

  constructor(logger: Logger) {
    super(logger)
  }

  contextKey = 'movie.directors'

  /**
   * Parses directors details from a HTML box and merges them into the given context's `movie` property.
   * @param box 
   * @param parsingConfig
   * @param context
   * @returns the merged movie object as in the context after the parsing
   */
  parse(box: cheerio.Cheerio, parsingConfig: DirectorsParsingConfig, context: ParsingContext) {
    MethodCallLogger.logMethodCall()
    context.pushCallstack()
    context.movie = context.movie || {}
    context.movie.directors = this.parseDirectors(box, parsingConfig, context)
    context.popCallstack()

    return context.movie.directors
  }

  parseDirectors(box: cheerio.Cheerio, parsingConfig: DirectorsParsingConfig, context: ParsingContext) {
    MethodCallLogger.logMethodCall()
    context.pushCallstack()
    let directors = context.movie?.directors || []
    if (parsingConfig.directors) {
      let valueGrabber = new ValueGrabber(parsingConfig.directors, this.logger, this.valueGrabberLogPrefix('directors'))
      let grabbedDirectors = valueGrabber?.grab(box, context)
      directors = _.chain([grabbedDirectors])
        .union(directors)
        .flatten()
        .compact()
        .uniq(false, (director: "string") => director.toLowerCase())
        .value()
    }
    context.popCallstack()
    return directors
  }

}