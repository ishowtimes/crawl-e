import { Movie } from '../models'

import Logger from '../Logger'
import MethodCallLogger from '../MethodCallLogger'
import Utils from '../Utils'
import { ItemParser } from './ItemParser'
import { ParsingContext } from './ParsingContext'
import { VersionParser, VersionParsingConfig } from './VersionParser'
import ValueGrabber, { ValueGrabbing } from '../ValueGrabber';
import { BaseParser } from './BaseParser';
import { DirectorsParsingConfig, DirectorsParser } from './DirectorsParser'
import { ReleaseDateParsingConfig, ReleaseDateParser } from './ReleaseDateParser';
import * as moment from 'moment';
import { TypeParsingConfig, TypeParser } from './TypeParser'
import { ImdbIdParsingConfig, ImdbIdParser } from './ImdbIdParser'
import { MoviePageLinkParsingConfig, MoviePageLinkParser } from './MoviePageLinkParser'

/**
 * MovieParsingConifg  with alternative value grabbers when parsing outside a movie context.
 * @category HTML Parsing
 */
export interface IndirectMovieParsingConfig {
  movieTitle?: ValueGrabbing
  movieTitleOriginal?: ValueGrabbing,
  type?: ValueGrabbing
}

/**
 * @category HTML Parsing
 */
export interface MovieParsingConfig extends IndirectMovieParsingConfig, VersionParsingConfig {
  id: ValueGrabbing
  href?: ValueGrabbing
  title: ValueGrabbing
  titleOriginal?: ValueGrabbing,
  releaseDate?: ValueGrabbing,
  releaseDateFormat?: moment.MomentFormatSpecification,
  releaseDateLocale?: string,
  directors?: ValueGrabbing,
  imdbId?: ValueGrabbing,
  moviePageLink?: ValueGrabbing
}

/**
 * @category HTML Parsing
 */
export class MovieParser extends BaseParser implements ItemParser<MovieParsingConfig | IndirectMovieParsingConfig> {
  constructor(logger: Logger, private versionParser: VersionParser, private directorsParser: DirectorsParser, private releaseDateParser: ReleaseDateParser) {
    super(logger)
  }

  contextKey = 'movie'

  parse(movieBox: cheerio.Cheerio, parsingConfig: MovieParsingConfig, context: ParsingContext) {
    MethodCallLogger.logMethodCall()
    context.pushCallstack()
    parsingConfig.title = parsingConfig.movieTitle || parsingConfig.title
    parsingConfig.titleOriginal = parsingConfig.movieTitleOriginal || parsingConfig.titleOriginal
    this.parsingConfig = parsingConfig
    context.movie = {} as Movie
    context.movie.id = this.grabProperty('id', movieBox, context)
    context.movie.title = this.grabProperty('title', movieBox, context)
    context.movie.titleOriginal = this.grabProperty('titleOriginal', movieBox, context)
    context.movie.href = this.grabProperty('href', movieBox, context)
    context.movie = Utils.compactObj(context.movie)

    let directorsParsingConfig: DirectorsParsingConfig = { directors: parsingConfig.directors }
    context.movie.directors = this.directorsParser.parseDirectors(movieBox, directorsParsingConfig, context)

    const typeParsingConfig: TypeParsingConfig = { type: parsingConfig.type }
    const typeParser = new TypeParser(this.logger)
    context.movie.type = typeParser.parseType(movieBox, typeParsingConfig, context) || this.grabProperty('type', movieBox, context)

    this.releaseDateParser = new ReleaseDateParser(this.logger)
    let releaseDateParsingConfig = this.releaseDateParsingConfig(parsingConfig)
    let releaseDate = this.releaseDateParser.parse(movieBox, releaseDateParsingConfig, context)
    let format = releaseDateParsingConfig?.releaseDateFormat

    let strFormat: string = format?.toString()
    let isValidMoment: boolean = moment.isMoment(releaseDate)
    let isValidReleaseDate: boolean = releaseDate.isValid()

    if (!isValidMoment) {
      this.logger.debug(`movies:selection`, `release date is not a valid moment value`)
    }
    else if (!isValidReleaseDate) {
      this.logger.debug(`movies:selection`, `skipping movies due to invalid release date`)
    }
    else if (strFormat) {
      switch (true) {
        case !!strFormat.match(/D+/):
          context.movie.releaseDate = releaseDate.format('YYYY-MM-DD')
          break
        case !!strFormat.match(/M+/):
          context.movie.releaseDate = releaseDate.format('YYYY-MM')
          break
        default:
          context.movie.releaseDate = releaseDate.format('YYYY')
          break
      }
    }
    else {
      this.logger.debug(`movies:selection`, `Unknown case for format of release date`)
    }

    if (parsingConfig.imdbId) {
      const imdbIdParsingConfig: ImdbIdParsingConfig = { imdbId: parsingConfig.imdbId }
      const imdbIdParser = new ImdbIdParser(this.logger)
      context.movie.imdbId = imdbIdParser.parse(movieBox, imdbIdParsingConfig, context) || this.grabProperty('imdbId', movieBox, context)
    }

    if (parsingConfig.moviePageLink) {
      const moviePageLinkParsingConfig: MoviePageLinkParsingConfig = { moviePageLink: parsingConfig.moviePageLink }
      const moviePageLinkParser = new MoviePageLinkParser(this.logger)
      context.movie.moviePageLink = moviePageLinkParser.parse(movieBox, moviePageLinkParsingConfig, context) || this.grabProperty('moviePageLink', movieBox, context)
    }

    context.movie.version = this.versionParser.parse(movieBox, parsingConfig, context)

    this.logger.debug(`movies:result`, context.movie)
    context.popCallstack()
    return context.movie
  }

  private releaseDateParsingConfig(moviesConfig: MovieParsingConfig): ReleaseDateParsingConfig {
    return {
      releaseDate: moviesConfig.releaseDate,
      releaseDateFormat: moviesConfig.releaseDateFormat || 'YYYY',
      releaseDateLocale: moviesConfig.releaseDateLocale || 'en'
    }
  }

}