module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
    mocha: true
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
    project: './tsconfig.json'
  },
  plugins: [
    '@typescript-eslint',
    'mocha',
    'chai-friendly',
    'promise',
    'simple-import-sort'
  ],
  rules: {
    '@typescript-eslint/consistent-type-imports': 'off',
    '@typescript-eslint/consistent-type-exports': 'off',
    'consistent-type-imports': 'off',
    'indent': 'off',
    '@typescript-eslint/indent': [
      'error',
      2
    ],
    'linebreak-style': [
      'error',
      'unix'
    ],
    'quotes': [
      'error',
      'single'
    ],
    'semi': 'off',
    '@typescript-eslint/semi': ['error', 'never'],
    'no-multi-spaces': 'error',
    'no-mixed-spaces-and-tabs': ['error'],
    'quote-props': ['error', 'consistent-as-needed'],
    'prefer-const': 'error',
    'no-trailing-spaces': [2, { skipBlankLines: true }],
    '@typescript-eslint/prefer-nullish-coalescing': 'off',
    '@typescript-eslint/strict-boolean-expressions': 0,
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/no-unused-vars': 'warn',
    '@typescript-eslint/restrict-plus-operands': 'off',
    '@typescript-eslint/consistent-type-assertions': 'warn',
    '@typescript-eslint/prefer-optional-chain': 'warn',
    '@typescript-eslint/no-var-requires': 'warn',
    'no-irregular-whitespace': ['warn', { skipStrings: false, skipComments: false, skipRegExps: true, skipTemplates: false }],
    '@typescript-eslint/restrict-template-expressions': 'warn',
    '@typescript-eslint/no-extraneous-class': 'warn',
    '@typescript-eslint/no-namespace': 'off',
    '@typescript-eslint/no-base-to-string': 'off',
    '@typescript-eslint/ban-ts-comment': 'warn',
    'no-return-assign': 'off',
    'func-call-spacing': 'off',
    '@typescript-eslint/func-call-spacing': ['error', 'never'],
    'space-before-function-paren': 'off',
    '@typescript-eslint/space-before-function-paren': ['error', { anonymous: 'always', named: 'never', asyncArrow: 'always' }],
    'no-empty-function': 'off',
    '@typescript-eslint/no-empty-function': 'warn',
    'ban-types': 'off',
    '@typescript-eslint/ban-types': 'warn',
    '@typescript-eslint/no-this-alias': 'warn',
    'no-useless-escape': 'warn',
    'no-control-regex': 'warn',
    '@typescript-eslint/promise-function-async': 'off'
  },
  ignorePatterns: [
    'node_modules/',
    '**/typings/*.d.ts',
    '**/spec/*.js',
    'src/config-schema.ts',
    'src/MethodCallLogger.ts',
    'src/cli-params.ts'
  ],
  overrides: [
    {
      files: ['*.test.ts', '*.*-test.ts'],
      rules: {
        'no-unused-expressions': 'off',
        '@typescript-eslint/no-unused-expressions': 'warn',
        'n/handle-callback-err': 'off',
        'array-callback-return': 'warn',
        'no-use-before-define': 'off',
        '@typescript-eslint/no-use-before-define': 'warn',
        '@typescript-eslint/naming-convention': 'warn'
      }
    }
  ]
}
