# Crawl-E

Showtimes crawler framework by Cinepass / internationalshowtimes.com

## Project Setup

The framework is writting in [TypeScript](typescriptlang.org) and compiled into Node.js Javascript for usage of the library.

[MochaJS](mochajs.org) + [ChaiJS's expect](http://chaijs.com/guide/styles/#expect) is used for unit testing.

### Directories

#### src
source directory for TypeScript code

#### lib
library directory for importing the compiled framework

#### spec
JSON Schema definitions for the config. Is use to validate the config as well as for generating parts of the documentation as a single source of truth.

#### docs
[Docsify](https://docsify.js.org/) based documentation of the framework.

Icons: http://build.prestashop.com/prestashop-icon-font/

#### tests
Contains additional tests as well as supporting files such as recorded http request via [nock](https://github.com/node-nock/nock#recording).
Simple unit tests of the components as next to them in the `src` directory.

## Docs release

Call below comand
```
yarn docker:docs:build
```

Push the image to docker registry and use it. Documentation not shared here as this repository is public.

## Release

Call below commands and make sure that everything is working fine. Commit changes on develop branch
```
yarn build
yarn release:prepare
```

git flow is in use so call

```
git flow release start "v0.6.0"
```
but please change v0.6.0 to your version.

Change version in package.json. Commit it on new release branch. DO NOT PUSH THIS to repo. Then run

```
yarn typedoc
yarn docs:bower
```

Also commit changes.

Then call
```
git flow release finish "v0.6.0"
```

Keep in mind to push new master and develop branhes and tags.